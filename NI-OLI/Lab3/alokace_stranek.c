/*
struct page* alloc_pages(gfp_mask, order)
struct page* alloc_page(gfp_mask)
unsigned long__get_free_pages(gfp_mask, order)
unsigned long __get_free_page(gfp_mask)
void __free_pages(page, order)
void free_pages(addr, order)
void __free_page(page)
void free_page(addr)
*/

#include <unistd.h>
#include <errno.h>
#include <linux/gfp.h>

int main(){

    unsigned long page;

    page = __get_free_pages(GFP_KERNEL, 3);
    
    if (!page) {
            return ENOMEM;
    }

    free_pages(page, 3);

    return 0;
}