#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <set>
#include <stack>
#include <queue>
#include <mpi.h>

#define MAX_BOARD_SIZE 20


int best_turn = -1, max_turns;
std::stack<std::pair<int, int>> best_result;
int board_size = 0;

class Moves {
public:
    Moves(int rook, int knight, int numOfPiecesLeft, const std::vector<char> &board,
          std::stack<std::pair<int, int>> &moves) : rook(rook), knight(knight),
                                                    num_of_pieces_left(
                                                            numOfPiecesLeft),
                                                    board(board),
                                                    moves(moves) {}

    int rook;
    int knight;
    int num_of_pieces_left;
    std::vector<char> board;
    std::stack<std::pair<int, int>> moves;
};

class ChessBoard {
public:
    explicit ChessBoard(const int size, const int max) {
        board.resize(size * size);
        num_of_filled_lines = 0;
        board_size = size;
        best_turn = max * 2;
    }

    void fill_line(const std::string line) {
        for (unsigned int i = 0; i < line.size(); i++) {
            switch (line[i]) {
                case 'P': {
                    num_of_pieces++;
                    break;
                }
                case 'V': {
                    rook = num_of_filled_positions;
                    break;
                }
                case 'J': {
                    knight = num_of_filled_positions;
                    break;
                }
            }
            board[num_of_filled_positions] = line[i];
            num_of_filled_positions++;
        }
        num_of_filled_lines++;
    }

    const int &getKnight() const {
        return knight;
    }

    const int &getRook() const {
        return rook;
    }

    std::vector<char> getBoard() const {
        return board;
    }

    int getNumOfPieces() const {
        return num_of_pieces;
    }

    const std::string getBestResult() const {
        auto tmp = best_result;
        std::string res = "";
        std::string move_repre;
        while (!tmp.empty()) {
            move_repre = "";
            auto pair = tmp.top();
            move_repre += "(" + std::to_string(pair.first) + "," + std::to_string(pair.second) + ")";
            if (board[pair.first * board_size + pair.second] == 'P')
                move_repre += "* ";
            else
                move_repre += " ";
            res = res + move_repre;
            tmp.pop();
        }
        return res;
    }

private:
    int num_of_filled_lines = 0;
    int num_of_filled_positions = 0;
    int num_of_pieces = 0;
    std::vector<char> board;
    int knight;
    int rook;
};


void print(std::vector<char> b) {
    for (int i = 0; i < board_size * board_size; i++) {
        if (i % board_size == 0)
            std::cout << std::endl;
        std::cout << b[i];

    }
    std::cout << std::endl;
}

std::vector<std::pair<int, int>> next(const int &turn, std::vector<char> &b, int &k_pos, int &r_pos) {
    std::vector<std::pair<int, int>> possible_moves;
    auto k = std::make_pair(k_pos / board_size, k_pos % board_size);
    auto r = std::make_pair(r_pos / board_size, r_pos % board_size);
    if (turn % 2) {
        //knight turn to move so we count all possible moves
        //checking all possible locations for knight to move
        //excluding all out of bounds moves first and then
        //excluding moves where you can step on rook
        //look down
        if (k.first + 2 < board_size) {
            auto probable_move = std::make_pair(k.first + 2, k.second + 1);
            if (k.second + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.second -= 2;
            if (k.second - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
        //look up
        if (k.first - 2 >= 0) {
            auto probable_move = std::make_pair(k.first - 2, k.second + 1);
            if (k.second + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.second -= 2;
            if (k.second - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
        //look right
        if (k.second + 2 < board_size) {
            auto probable_move = std::make_pair(k.first + 1, k.second + 2);
            if (k.first + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.first -= 2;
            if (k.first - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
        //look left
        if (k.second - 2 >= 0) {
            auto probable_move = std::make_pair(k.first + 1, k.second - 2);
            if (k.first + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.first -= 2;
            if (k.first - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
    } else {
        //rook turn to move so we count all possible moves
        //all empty spaces + first found position of P are included
        //look down
        for (int i = r.first; i < board_size; i++) {
            auto probable_move = std::make_pair(i + 1, r.second);
            if (i + 1 >= board_size)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
        //look up
        for (int i = r.first; i > 0; i--) {
            auto probable_move = std::make_pair(i - 1, r.second);
            if (i - 1 < 0)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
        //look right
        for (int i = r.second; i < board_size; i++) {
            auto probable_move = std::make_pair(r.first, i + 1);
            if (i + 1 >= board_size)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
        //look left
        for (int i = r.second; i > 0; i--) {
            auto probable_move = std::make_pair(r.first, i - 1);
            if (i - 1 < 0)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
    }
    return possible_moves;
}

std::vector<std::pair<std::pair<int, int>, int>>
val(const int &turn, const std::vector<std::pair<int, int>> &possible_moves, std::vector<char> &b) {
    std::vector<std::pair<std::pair<int, int>, int>> eval_moves;
    if (turn % 2) {
        //knight move
        for (auto move : possible_moves) {
            if (b[move.first * board_size + move.second] == 'P')
                eval_moves.emplace_back(std::make_pair(move, 2));
            else
                eval_moves.emplace_back(std::make_pair(move, 0));
        }
    } else {
        //rook move
        for (auto move : possible_moves) {
            //if on possible move is P we put priority 2
            if (b[move.first * board_size + move.second] == 'P')
                eval_moves.emplace_back(std::make_pair(move, 2));
            else {
                int next_move_take = 0;
                //search through the col of possible move for some P
                for (int i = 0; i < board_size; i++) {
                    if (next_move_take)
                        break;
                    //if there is P in same col then it makes sense to move there for next turn
                    if (b[i * board_size + move.second] == 'P')
                        next_move_take = 1;
                }
                //search through the row of possible move for some P
                for (int i = 0; i < board_size; i++) {
                    if (next_move_take)
                        break;
                    //if there is P in same row the moving there makes sense
                    if (b[move.first * board_size + i] == 'P')
                        next_move_take = 1;
                }
                //if we found a move to go where we can take P next turn we insert 1 instead of 0
                if (next_move_take)
                    eval_moves.emplace_back(std::make_pair(move, 1));
                else
                    eval_moves.emplace_back(std::make_pair(move, 0));
            }
        }
    }
    return eval_moves;
}

void
solve(int turn, int k, int r, std::vector<char> b, int pieces_left,
      std::stack<std::pair<int, int>> result) {
    //if recursion takes longer than max threshold we cut it
    if (turn >= max_turns)
        return;

    if (pieces_left == 0 && turn < best_turn) {
#pragma omp critical
        {
            if (turn < best_turn) {
                best_turn = turn;
                best_result = result;
                //std::cout << "found solution in " << best_turn << " turns" << std::endl;
            }
        };

        return;
    }
    if (turn + pieces_left >= best_turn || turn + pieces_left >= max_turns)
        return;

    //finding possible moves and evaluating them
    auto possible_moves = next(turn, b, k, r);
    auto eval_moves = val(turn, possible_moves, b);
    std::sort(eval_moves.begin(), eval_moves.end(), [](auto a, auto b) { return a.second > b.second; });
    for (auto move : eval_moves) {
        //making a copy for every move recursion
        auto new_pieces_left = pieces_left;
        auto new_k = k;
        auto new_r = r;

        //if move takes piece
        if (b[move.first.first * board_size + move.first.second] == 'P')
            new_pieces_left--;

        //moving stuff on board according who has turn
        if (turn % 2) {
            //knight turn
            b[k] = '-';
            new_k = move.first.first * board_size + move.first.second;
            b[move.first.first * board_size + move.first.second] = 'J';
        } else {
            //rook turn
            b[r] = '-';
            new_r = move.first.first * board_size + move.first.second;
            b[move.first.first * board_size + move.first.second] = 'V';
        }
        result.push(move.first);
//#pragma omp task
        solve(turn + 1, new_k, new_r, b, new_pieces_left, result);

        result.pop();

        //reverting changes for other moves
        if (pieces_left > new_pieces_left)
            b[move.first.first * board_size + move.first.second] = 'P';
        else
            b[move.first.first * board_size + move.first.second] = '-';
        if (turn % 2)
            b[k] = 'J';
        else
            b[r] = 'V';
    }
    return;
}


std::vector<Moves> getMoves(int turn, int k, int r, std::vector<char> b, int pieces_left) {
    std::vector<Moves> returnValue;
    //finding possible moves and evaluating them
    auto possible_moves = next(turn, b, k, r);
    std::stack<std::pair<int, int>> mov;
    for (auto move : possible_moves) {
        //making a copy for every move recursion
        auto new_pieces_left = pieces_left;
        auto new_k = k;
        auto new_r = r;
        bool r_took_piece = false;

        //if move takes piece
        if (b[move.first * board_size + move.second] == 'P') {
            r_took_piece = true;
            new_pieces_left--;
        }

        //rook turn
        b[r] = '-';
        new_r = move.first * board_size + move.second;
        b[move.first * board_size + move.second] = 'V';

        mov.push(move);
        auto k_turn = turn + 1;
        auto possbile_knight_turns = next(k_turn, b, k, new_r);
        for (auto k_move : possbile_knight_turns) {
            bool k_took_piece = false;
            if (b[k_move.first * board_size + k_move.second] == 'P') {
                k_took_piece = true;
                new_pieces_left--;
            }
            //knight turn
            b[k] = '-';
            new_k = k_move.first * board_size + k_move.second;
            b[k_move.first * board_size + k_move.second] = 'J';
            //push the preset state to vector
            mov.push(k_move);
            returnValue.push_back(Moves(new_r, new_k, new_pieces_left, b, mov));

            //reverting changes for other moves
            if (k_took_piece) {
                b[k_move.first * board_size + k_move.second] = 'P';
                new_pieces_left++;
            } else
                b[k_move.first * board_size + k_move.second] = '-';
            b[k] = 'J';
            mov.pop();
        }

        //reverting changes for other moves
        if (r_took_piece) {
            b[move.first * board_size + move.second] = 'P';
            new_pieces_left++;
        } else
            b[move.first * board_size + move.second] = '-';
        b[r] = 'V';
        mov.pop();
    }
    return returnValue;
}

int main(int argc, char **argv) {

    //reading input and precounting moves
    std::fstream fs;
    fs.open(argv[1]);
    int k;
    std::string line;
    char *pEnd;
    fs >> line;
    k = std::strtol(line.c_str(), &pEnd, 10);
    fs >> line;
    max_turns = std::strtol(line.c_str(), &pEnd, 10);
    ChessBoard board(k, max_turns);
    //std::cout << k << " " << max_turns << std::endl;
    while (!fs.eof()) {
        fs >> line;
        board.fill_line(line);
    }
    //print(board.getBoard());

    /* start up MPI */
    MPI_Init(&argc, &argv);

    // start of MPI
    int my_rank;
    int p;
    MPI_Status status;
    int vy_ste_kral_ja_vas_volit_nebudu = 1;
    int job_done = 2;
    int end_tag = 69;

    /* find out process rank */
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    /* find out number of processes */
    MPI_Comm_size(MPI_COMM_WORLD, &p);

    if (my_rank != 0){
        int knig,r,num_of_pieces_left;
        auto b = new char[MAX_BOARD_SIZE*MAX_BOARD_SIZE];
        for(int i = 0; i < MAX_BOARD_SIZE*MAX_BOARD_SIZE;i++)
            b[i] = '+';
        auto movero = new int [4];
        while (true) {
            //waiting for message from master
            //std::cout << "(" << my_rank << ") " << "Waiting for moves" << std::endl;
            MPI_Recv(&b[0], MAX_BOARD_SIZE*MAX_BOARD_SIZE, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            if (status.MPI_TAG == end_tag) {
                //std::cout << "(" << my_rank << ") " << "Dead" << std::endl;
                //std::cout << "(" << my_rank << ") " << board.getBestResult() << " " << best_turn << std::endl;
                auto arr_size = best_result.size()*2+1;
                auto move_arr = new int[arr_size];
                int j = 0;
                while(!best_result.empty()){
                    move_arr[j++] = best_result.top().first;
                    move_arr[j++] = best_result.top().second;
                    best_result.pop();
                }
                move_arr[j] = best_turn;
                MPI_Send(&move_arr[0], arr_size, MPI_INT, 0, end_tag, MPI_COMM_WORLD);
                break;
                //in other case we recieved data to process so we have a job to do for master, after it we send job_done tag
            }
            MPI_Recv(&movero[0], 4, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(&knig, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(&r, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(&num_of_pieces_left, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            //std::cout << "(" << my_rank << ") " << "Got message! " << status.MPI_SOURCE << " " << status.MPI_TAG << " " << status.MPI_ERROR << std::endl;
            //if ending tag sent, slave sends best result to master and ends

                std::stack<std::pair<int,int>> tmp_moves;
                tmp_moves.push(std::make_pair(movero[0],movero[1]));
                tmp_moves.push(std::make_pair(movero[2],movero[3]));
                std::vector<char> tmp_vec_board;
                for(int i = 0 ; b[i] != '+' ;i++)
                    tmp_vec_board.push_back(b[i]);
                //std::cout << "(" << my_rank << ") " << "Working" << std::endl;
#pragma omp parallel num_threads(2)
                {
#pragma omp single
                    solve(2, knig, r, tmp_vec_board, num_of_pieces_left, tmp_moves);
                }
                MPI_Send(&my_rank, 1, MPI_INT, 0, job_done, MPI_COMM_WORLD);

        }
    } else {

        auto moves = getMoves(0, board.getKnight(), board.getRook(), board.getBoard(), board.getNumOfPieces());

        std::cout << "MPI process count: " << p << std::endl;
        std::queue<int> lazy_slaves;
        //making a queue of MY slaves :)
        for (int i = 1; i < p; ++i)
            lazy_slaves.push(i);

        //sending work to slaves
        auto move_arr = new int[4];
        for (unsigned int i = 0; i < moves.size(); i++) {
            int slave_id = lazy_slaves.front();
            lazy_slaves.pop();
            //sending a job to do
            //std::cout << "(" << my_rank << ") " << "SENDING JOB " << i << " TO " << slave_id << std::endl;
            //TODO serializace
            auto char_board = new char [moves[i].board.size()];
            int iterka = 0;
            for(auto elem: moves[i].board)
                char_board[iterka++] = elem;
            MPI_Send(&char_board[0], moves[i].board.size(), MPI_CHAR, slave_id, vy_ste_kral_ja_vas_volit_nebudu, MPI_COMM_WORLD);
            int j = 0;
            while(!moves[i].moves.empty()){
                move_arr[j++] = moves[i].moves.top().first;
                move_arr[j++] = moves[i].moves.top().second;
                moves[i].moves.pop();
            }
            MPI_Send(&move_arr[0], 4, MPI_INT, slave_id, vy_ste_kral_ja_vas_volit_nebudu, MPI_COMM_WORLD);
            MPI_Send(&moves[i].knight, 1, MPI_INT, slave_id, vy_ste_kral_ja_vas_volit_nebudu, MPI_COMM_WORLD);
            MPI_Send(&moves[i].rook, 1, MPI_INT, slave_id, vy_ste_kral_ja_vas_volit_nebudu, MPI_COMM_WORLD);
            MPI_Send(&moves[i].num_of_pieces_left, 1, MPI_INT, slave_id, vy_ste_kral_ja_vas_volit_nebudu, MPI_COMM_WORLD);
            //if no slaves available
            if (lazy_slaves.empty()) {
                //std::cout << "(" << my_rank << ") " << "NO LAZY SLAVES number of moves left: " << moves.size()-i << std::endl;
                int lazy_slave_id;
                //waiting to get some free slave
                MPI_Recv(&lazy_slave_id, sizeof(lazy_slave_id), MPI_INT, MPI_ANY_SOURCE, job_done, MPI_COMM_WORLD,
                         MPI_STATUS_IGNORE);
                lazy_slaves.push(lazy_slave_id);
            }
        }
        //ending all slaves
        for (int slave_ranks = 1; slave_ranks < p; ++slave_ranks){
            //std::cout << "(" << my_rank << ") " << "Ending slaves" << std::endl;
            MPI_Send(&end_tag, 1, MPI_INT, slave_ranks, 69,  MPI_COMM_WORLD);
        }
        //receiving their best results
        auto best_slave_message = new int[max_turns*2+3];
        int slavesCount = p - 1;
        //while cycle until all slaves send response back with their best results
        while (slavesCount > 0) {
            for(int i = 0; i < max_turns*2+3;i++)
                best_slave_message[i] = -1;
            //std::cout << "(" << my_rank << ") " << "Waiting for slave response" << std::endl;
            MPI_Recv(&best_slave_message[0], max_turns*2+3, MPI_INT, MPI_ANY_SOURCE, 69, MPI_COMM_WORLD, &status);
            /*for(int i = 0; i < max_turns*2+3; i++)
                std::cout << best_slave_message[i];
            std::cout << std::endl;*/
            slavesCount--;
            int tmp_best_turn_slave;
            std::stack<std::pair<int,int>> tmp_stack;
            for(unsigned i = 0;best_slave_message[i] >= 0;i+=2){
                if(best_slave_message[i+1] == -1){
                    tmp_best_turn_slave = best_slave_message[i];
                    break;
                }
                tmp_stack.push(std::make_pair(best_slave_message[i],best_slave_message[i+1]));
            }
            //std::cout << "(" << my_rank << ") " << "GOT MESSAGE FROM " << status.MPI_SOURCE << " with solution: " << tmp_best_turn_slave << std::endl;
            //comparing slaves best result to masters best result
            if (best_turn == -1 || tmp_best_turn_slave < best_turn) {
                best_turn = tmp_best_turn_slave;
                best_result = tmp_stack;
            }
        }
        std::cout << "Best solution is done in " << best_turn << " turns" << std::endl << board.getBestResult()
                  << std::endl;
    }
    /* shut down MPI */
    MPI_Finalize();
}