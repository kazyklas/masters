#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <set>
#include <stack>

long call_counter = 0;

class ChessBoard {
public:
    explicit ChessBoard(const int size, const int max) {
        board = new char[size * size];
        num_of_filled_lines = 0;
        board_size = size;
        best_turn = max * 2;
    }

    void fill_line(const std::string line) {
        for (unsigned int i = 0; i < line.size(); i++) {
            switch (line[i]) {
                case 'P': {
                    num_of_pieces++;
                    break;
                }
                case 'V': {
                    rook = num_of_filled_positions;
                    break;
                }
                case 'J': {
                    knight = num_of_filled_positions;
                    break;
                }
            }
            board[num_of_filled_positions] = line[i];
            num_of_filled_positions++;
        }
        num_of_filled_lines++;
    }

    void print(char *b) {
        for (int i = 0; i < board_size * board_size; i++) {
            if (i % board_size == 0)
                std::cout << std::endl;
            std::cout << b[i];

        }
        std::cout << std::endl;
    }

    void
    solve(const int &max_turns, int turn, int k, int r, char *b, int pieces_left,
          std::stack<std::pair<int, int>> &result) {
        call_counter++;
        //std::cout << "turn:" << turn << std::endl;
        //if recursion takes longer than max threshold we cut it
        if (turn >= max_turns) {
            return;
        }
        //my new best
        if (pieces_left == 0 && turn < best_turn) {
            best_turn = turn;
            best_result = result;
            std::cout << "found solution in " << best_turn << " turns" << std::endl;
            return;
        }
        //some recursion finished
        if (pieces_left == 0)
            return;
        //recursion took too many turns to do it
        if (turn + 1 >= best_turn)
            return;
        if (turn + pieces_left >= best_turn || turn + pieces_left >= max_turns)
            return;

        //finding possible moves and evaluating them
        auto possible_moves = next(turn, b, k, r);
        auto eval_moves = val(turn, possible_moves, b);
        std::sort(eval_moves.begin(), eval_moves.end(), [](auto a, auto b) { return a.second > b.second; });
        std::string move_representation;
        for (auto move : eval_moves) {
            //making a copy for every move recursion
            auto new_pieces_left = pieces_left;

            //if move takes piece
            if (b[move.first.first * board_size + move.first.second] == 'P')
                new_pieces_left--;

            //moving stuff on board according who has turn
            if (turn % 2) {
                //knight turn
                auto new_k = k;
                b[k] = '-';
                new_k = move.first.first * board_size + move.first.second;
                b[move.first.first * board_size + move.first.second] = 'J';
                result.push(move.first);
                //print(b);
                solve(max_turns, turn + 1, new_k, r, b, new_pieces_left, result);
            } else {
                //rook turn
                auto new_r = r;
                b[r] = '-';
                new_r = move.first.first * board_size + move.first.second;
                b[move.first.first * board_size + move.first.second] = 'V';
                result.push(move.first);
                //print(b);
                solve(max_turns, turn + 1, k, new_r, b, new_pieces_left, result);
            }

            result.pop();

            //reverting changes for other moves
            if (pieces_left > new_pieces_left)
                b[move.first.first * board_size + move.first.second] = 'P';
            else
                b[move.first.first * board_size + move.first.second] = '-';
            if (turn % 2) {
                //knight turn
                b[k] = 'J';
            } else {
                //rook turn
                b[r] = 'V';
            }
        }
        return;
    }

    const int &getKnight() const {
        return knight;
    }

    const int &getRook() const {
        return rook;
    }

    char *getBoard() const {
        return board;
    }

    int getNumOfPieces() const {
        return num_of_pieces;
    }

    const std::string getBestResult() const {
        auto tmp = best_result;
        std::string res = "";
        std::string move_repre;
        while (!tmp.empty()) {
            move_repre = "";
            auto pair = tmp.top();
            move_repre += "(" + std::to_string(pair.first) + "," + std::to_string(pair.second) + ")";
            if (board[pair.first * board_size + pair.second] == 'P')
                move_repre += "* ";
            else
                move_repre += " ";
            res = move_repre + res;
            tmp.pop();
        }
        return res;
    }

private:
    std::vector<std::pair<int, int>> next(const int &turn, char *&b, int &k_pos, int &r_pos) {
        std::vector<std::pair<int, int>> possible_moves;
        auto k = std::make_pair(k_pos / board_size, k_pos % board_size);
        auto r = std::make_pair(r_pos / board_size, r_pos % board_size);
        if (turn % 2) {
            //knight turn to move so we count all possible moves
            //checking all possible locations for knight to move
            //excluding all out of bounds moves first and then
            //excluding moves where you can step on rook
            //look down
            if (k.first + 2 < board_size) {
                auto probable_move = std::make_pair(k.first + 2, k.second + 1);
                if (k.second + 1 < board_size)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
                probable_move.second -= 2;
                if (k.second - 1 >= 0)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
            }
            //look up
            if (k.first - 2 >= 0) {
                auto probable_move = std::make_pair(k.first - 2, k.second + 1);
                if (k.second + 1 < board_size)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
                probable_move.second -= 2;
                if (k.second - 1 >= 0)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
            }
            //look right
            if (k.second + 2 < board_size) {
                auto probable_move = std::make_pair(k.first + 1, k.second + 2);
                if (k.first + 1 < board_size)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
                probable_move.first -= 2;
                if (k.first - 1 >= 0)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
            }
            //look left
            if (k.second - 2 >= 0) {
                auto probable_move = std::make_pair(k.first + 1, k.second - 2);
                if (k.first + 1 < board_size)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
                probable_move.first -= 2;
                if (k.first - 1 >= 0)
                    if (b[probable_move.first * board_size + probable_move.second] != 'V')
                        possible_moves.emplace_back(probable_move);
            }
        } else {
            //rook turn to move so we count all possible moves
            //all empty spaces + first found position of P are included
            //look down
            for (int i = r.first; i < board_size; i++) {
                auto probable_move = std::make_pair(i + 1, r.second);
                if (i + 1 >= board_size)
                    break;
                if (b[probable_move.first * board_size + probable_move.second] == '-')
                    possible_moves.emplace_back(probable_move);
                else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                    possible_moves.emplace_back(probable_move);
                    break;
                } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                    break;
            }
            //look up
            for (int i = r.first; i > 0; i--) {
                auto probable_move = std::make_pair(i - 1, r.second);
                if (i - 1 < 0)
                    break;
                if (b[probable_move.first * board_size + probable_move.second] == '-')
                    possible_moves.emplace_back(probable_move);
                else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                    possible_moves.emplace_back(probable_move);
                    break;
                } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                    break;
            }
            //look right
            for (int i = r.second; i < board_size; i++) {
                auto probable_move = std::make_pair(r.first, i + 1);
                if (i + 1 >= board_size)
                    break;
                if (b[probable_move.first * board_size + probable_move.second] == '-')
                    possible_moves.emplace_back(probable_move);
                else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                    possible_moves.emplace_back(probable_move);
                    break;
                } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                    break;
            }
            //look left
            for (int i = r.second; i > 0; i--) {
                auto probable_move = std::make_pair(r.first, i - 1);
                if (i - 1 < 0)
                    break;
                if (b[probable_move.first * board_size + probable_move.second] == '-')
                    possible_moves.emplace_back(probable_move);
                else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                    possible_moves.emplace_back(probable_move);
                    break;
                } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                    break;
            }
        }
        return possible_moves;
    }

    std::vector<std::pair<std::pair<int, int>, int>>
    val(const int &turn, const std::vector<std::pair<int, int>> &possible_moves, char *&b) {
        std::vector<std::pair<std::pair<int, int>, int>> eval_moves;
        if (turn % 2) {
            //knight move
            for (auto move : possible_moves) {
                if (b[move.first * board_size + move.second] == 'P')
                    eval_moves.emplace_back(std::make_pair(move, 2));
                else
                    eval_moves.emplace_back(std::make_pair(move, 0));
            }
        } else {
            //rook move
            for (auto move : possible_moves) {
                //if on possible move is P we put priority 2
                if (b[move.first * board_size + move.second] == 'P')
                    eval_moves.emplace_back(std::make_pair(move, 2));
                else {
                    int next_move_take = 0;
                    //search through the col of possible move for some P
                    for (int i = 0; i < board_size; i++) {
                        if (next_move_take)
                            break;
                        //if there is P in same col then it makes sense to move there for next turn
                        if (b[i * board_size + move.second] == 'P')
                            next_move_take = 1;
                    }
                    //search through the row of possible move for some P
                    for (int i = 0; i < board_size; i++) {
                        if (next_move_take)
                            break;
                        //if there is P in same row the moving there makes sense
                        if (b[move.first * board_size + i] == 'P')
                            next_move_take = 1;
                    }
                    //if we found a move to go where we can take P next turn we insert 1 instead of 0
                    if (next_move_take)
                        eval_moves.emplace_back(std::make_pair(move, 1));
                    else
                        eval_moves.emplace_back(std::make_pair(move, 0));
                }
            }
        }
        return eval_moves;
    }

    int num_of_filled_lines = 0;
    int num_of_filled_positions = 0;
    int num_of_pieces = 0;
    int board_size = 0;
    char *board;
    int knight;
    int rook;
    int best_turn;
    std::stack<std::pair<int, int>> best_result;
};


int main() {
    std::fstream fs;
    fs.open("../vaj/vaj11.txt");
    int k, max;
    std::string line;
    char *pEnd;
    fs >> line;
    k = std::strtol(line.c_str(), &pEnd, 10);
    fs >> line;
    max = std::strtol(line.c_str(), &pEnd, 10);
    ChessBoard board(k, max);
    std::cout << k << " " << max << std::endl;
    while (!fs.eof()) {
        fs >> line;
        board.fill_line(line);
    }
    board.print(board.getBoard());
    auto start = std::chrono::high_resolution_clock::now();
    std::stack<std::pair<int, int>> dummyResult;
    board.solve(max, 0, board.getKnight(), board.getRook(), board.getBoard(),
                board.getNumOfPieces(), dummyResult);
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << call_counter << std::endl;
    std::cout << duration << std::endl;
    std::cout << board.getBestResult() << std::endl;
}
