#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <set>
#include <stack>


int best_turn, max_turns;
std::stack<std::pair<int, int>> best_result;
int board_size = 0;

class Moves {
public:
    Moves(int rook, int knight, int numOfPiecesLeft, const std::vector<char> &board, std::stack<std::pair<int, int>> &moves) : rook(rook), knight(knight),
                                                                                       num_of_pieces_left(
                                                                                               numOfPiecesLeft),
                                                                                       board(board),
                                                                                       moves(moves) {}

    int rook;
    int knight;
    int num_of_pieces_left;
    std::vector<char> board;
    std::stack<std::pair<int, int>> moves;
};

class ChessBoard {
public:
    explicit ChessBoard(const int size, const int max) {
        board.resize(size * size);
        num_of_filled_lines = 0;
        board_size = size;
        best_turn = max * 2;
    }

    void fill_line(const std::string line) {
        for (unsigned int i = 0; i < line.size(); i++) {
            switch (line[i]) {
                case 'P': {
                    num_of_pieces++;
                    break;
                }
                case 'V': {
                    rook = num_of_filled_positions;
                    break;
                }
                case 'J': {
                    knight = num_of_filled_positions;
                    break;
                }
            }
            board[num_of_filled_positions] = line[i];
            num_of_filled_positions++;
        }
        num_of_filled_lines++;
    }

    const int &getKnight() const {
        return knight;
    }

    const int &getRook() const {
        return rook;
    }

    std::vector<char> getBoard() const {
        return board;
    }

    int getNumOfPieces() const {
        return num_of_pieces;
    }

    const std::string getBestResult() const {
        auto tmp = best_result;
        std::string res = "";
        std::string move_repre;
        while (!tmp.empty()) {
            move_repre = "";
            auto pair = tmp.top();
            move_repre += "(" + std::to_string(pair.first) + "," + std::to_string(pair.second) + ")";
            if (board[pair.first * board_size + pair.second] == 'P')
                move_repre += "* ";
            else
                move_repre += " ";
            res = move_repre + res;
            tmp.pop();
        }
        return res;
    }

private:
    int num_of_filled_lines = 0;
    int num_of_filled_positions = 0;
    int num_of_pieces = 0;
    std::vector<char> board;
    int knight;
    int rook;
};


void print(std::vector<char> b) {
    for (int i = 0; i < board_size * board_size; i++) {
        if (i % board_size == 0)
            std::cout << std::endl;
        std::cout << b[i];

    }
    std::cout << std::endl;
}

std::vector<std::pair<int, int>> next(const int &turn, std::vector<char> &b, int &k_pos, int &r_pos) {
    std::vector<std::pair<int, int>> possible_moves;
    auto k = std::make_pair(k_pos / board_size, k_pos % board_size);
    auto r = std::make_pair(r_pos / board_size, r_pos % board_size);
    if (turn % 2) {
        //knight turn to move so we count all possible moves
        //checking all possible locations for knight to move
        //excluding all out of bounds moves first and then
        //excluding moves where you can step on rook
        //look down
        if (k.first + 2 < board_size) {
            auto probable_move = std::make_pair(k.first + 2, k.second + 1);
            if (k.second + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.second -= 2;
            if (k.second - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
        //look up
        if (k.first - 2 >= 0) {
            auto probable_move = std::make_pair(k.first - 2, k.second + 1);
            if (k.second + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.second -= 2;
            if (k.second - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
        //look right
        if (k.second + 2 < board_size) {
            auto probable_move = std::make_pair(k.first + 1, k.second + 2);
            if (k.first + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.first -= 2;
            if (k.first - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
        //look left
        if (k.second - 2 >= 0) {
            auto probable_move = std::make_pair(k.first + 1, k.second - 2);
            if (k.first + 1 < board_size)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
            probable_move.first -= 2;
            if (k.first - 1 >= 0)
                if (b[probable_move.first * board_size + probable_move.second] != 'V')
                    possible_moves.emplace_back(probable_move);
        }
    } else {
        //rook turn to move so we count all possible moves
        //all empty spaces + first found position of P are included
        //look down
        for (int i = r.first; i < board_size; i++) {
            auto probable_move = std::make_pair(i + 1, r.second);
            if (i + 1 >= board_size)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
        //look up
        for (int i = r.first; i > 0; i--) {
            auto probable_move = std::make_pair(i - 1, r.second);
            if (i - 1 < 0)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
        //look right
        for (int i = r.second; i < board_size; i++) {
            auto probable_move = std::make_pair(r.first, i + 1);
            if (i + 1 >= board_size)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
        //look left
        for (int i = r.second; i > 0; i--) {
            auto probable_move = std::make_pair(r.first, i - 1);
            if (i - 1 < 0)
                break;
            if (b[probable_move.first * board_size + probable_move.second] == '-')
                possible_moves.emplace_back(probable_move);
            else if (b[probable_move.first * board_size + probable_move.second] == 'P') {
                possible_moves.emplace_back(probable_move);
                break;
            } else if (b[probable_move.first * board_size + probable_move.second] == 'J')
                break;
        }
    }
    return possible_moves;
}

std::vector<std::pair<std::pair<int, int>, int>>
val(const int &turn, const std::vector<std::pair<int, int>> &possible_moves, std::vector<char> &b) {
    std::vector<std::pair<std::pair<int, int>, int>> eval_moves;
    if (turn % 2) {
        //knight move
        for (auto move : possible_moves) {
            if (b[move.first * board_size + move.second] == 'P')
                eval_moves.emplace_back(std::make_pair(move, 2));
            else
                eval_moves.emplace_back(std::make_pair(move, 0));
        }
    } else {
        //rook move
        for (auto move : possible_moves) {
            //if on possible move is P we put priority 2
            if (b[move.first * board_size + move.second] == 'P')
                eval_moves.emplace_back(std::make_pair(move, 2));
            else {
                int next_move_take = 0;
                //search through the col of possible move for some P
                for (int i = 0; i < board_size; i++) {
                    if (next_move_take)
                        break;
                    //if there is P in same col then it makes sense to move there for next turn
                    if (b[i * board_size + move.second] == 'P')
                        next_move_take = 1;
                }
                //search through the row of possible move for some P
                for (int i = 0; i < board_size; i++) {
                    if (next_move_take)
                        break;
                    //if there is P in same row the moving there makes sense
                    if (b[move.first * board_size + i] == 'P')
                        next_move_take = 1;
                }
                //if we found a move to go where we can take P next turn we insert 1 instead of 0
                if (next_move_take)
                    eval_moves.emplace_back(std::make_pair(move, 1));
                else
                    eval_moves.emplace_back(std::make_pair(move, 0));
            }
        }
    }
    return eval_moves;
}


void
solve(int turn, int k, int r, std::vector<char> b, int pieces_left,
      std::stack<std::pair<int, int>> result) {
    //if recursion takes longer than max threshold we cut it
    if (turn >= max_turns)
        return;

    if (pieces_left == 0 && turn < best_turn) {
#pragma omp critical
        {
            if (turn < best_turn) {
                best_turn = turn;
                best_result = result;
                std::cout << "found solution in " << best_turn << " turns" << std::endl;
            }
        };

        return;
    }
    if (turn + pieces_left >= best_turn || turn + pieces_left >= max_turns)
        return;

    //finding possible moves and evaluating them
    auto possible_moves = next(turn, b, k, r);
    auto eval_moves = val(turn, possible_moves, b);
    std::sort(eval_moves.begin(), eval_moves.end(), [](auto a, auto b) { return a.second > b.second; });
    for (auto move : eval_moves) {
        //making a copy for every move recursion
        auto new_pieces_left = pieces_left;
        auto new_k = k;
        auto new_r = r;

        //if move takes piece
        if (b[move.first.first * board_size + move.first.second] == 'P')
            new_pieces_left--;

        //moving stuff on board according who has turn
        if (turn % 2) {
            //knight turn
            b[k] = '-';
            new_k = move.first.first * board_size + move.first.second;
            b[move.first.first * board_size + move.first.second] = 'J';
        } else {
            //rook turn
            b[r] = '-';
            new_r = move.first.first * board_size + move.first.second;
            b[move.first.first * board_size + move.first.second] = 'V';
        }
        result.push(move.first);
        solve(turn + 1, new_k, new_r, b, new_pieces_left, result);

        result.pop();

        //reverting changes for other moves
        if (pieces_left > new_pieces_left)
            b[move.first.first * board_size + move.first.second] = 'P';
        else
            b[move.first.first * board_size + move.first.second] = '-';
        if (turn % 2)
            b[k] = 'J';
        else
            b[r] = 'V';
    }
    return;
}


std::vector<Moves> getMoves(int turn, int k, int r, std::vector<char> b, int pieces_left) {
    std::vector<Moves> returnValue;
    //finding possible moves and evaluating them
    auto possible_moves = next(turn, b, k, r);
    std::stack<std::pair<int, int>> mov;
    for (auto move : possible_moves) {
        //making a copy for every move recursion
        auto new_pieces_left = pieces_left;
        auto new_k = k;
        auto new_r = r;
        bool r_took_piece = false;

        //if move takes piece
        if (b[move.first * board_size + move.second] == 'P'){
            r_took_piece = true;
            new_pieces_left--;
        }

        //rook turn
        b[r] = '-';
        new_r = move.first * board_size + move.second;
        b[move.first * board_size + move.second] = 'V';

        mov.push(move);
        auto k_turn = turn + 1;
        auto possbile_knight_turns = next(k_turn, b, k, new_r);
        for(auto k_move : possbile_knight_turns){
            bool k_took_piece = false;
            if (b[k_move.first * board_size + k_move.second] == 'P'){
                k_took_piece = true;
                new_pieces_left--;
            }
            //knight turn
            b[k] = '-';
            new_k = k_move.first * board_size + k_move.second;
            b[k_move.first * board_size + k_move.second] = 'J';
            //push the preset state to vector
            mov.push(k_move);
            returnValue.push_back(Moves(new_r,new_k,new_pieces_left,b,mov));

            //reverting changes for other moves
            if (k_took_piece){
                b[k_move.first * board_size + k_move.second] = 'P';
                new_pieces_left ++;
            }
            else
                b[k_move.first * board_size + k_move.second] = '-';
            b[k] = 'J';
            mov.pop();
        }

        //reverting changes for other moves
        if (r_took_piece){
            b[move.first * board_size + move.second] = 'P';
            new_pieces_left ++;
        }
        else
            b[move.first * board_size + move.second] = '-';
        b[r] = 'V';
        mov.pop();
    }
    return returnValue;
}

int main() {
    std::fstream fs;
    fs.open("../vaj/vaj2.txt");
    int k;
    std::string line;
    char *pEnd;
    fs >> line;
    k = std::strtol(line.c_str(), &pEnd, 10);
    fs >> line;
    max_turns = std::strtol(line.c_str(), &pEnd, 10);
    ChessBoard board(k, max_turns);
    std::cout << k << " " << max_turns << std::endl;
    while (!fs.eof()) {
        fs >> line;
        board.fill_line(line);
    }
    print(board.getBoard());
    auto moves = getMoves(0,board.getKnight(),board.getRook(),board.getBoard(),board.getNumOfPieces());
    std::cout << "Number of generated moves: "<< moves.size() << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::stack<std::pair<int, int>> startingMoves;
#pragma omp parallel for
    for(unsigned int i = 0; i < moves.size(); i++){
        solve(2, moves[i].knight, moves[i].rook, moves[i].board,
              moves[i].num_of_pieces_left, moves[i].moves);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << duration << std::endl;
    std::cout << board.getBestResult() << std::endl;
}
