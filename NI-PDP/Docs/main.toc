\selectlanguage *{latex}
\contentsline {section}{\numberline {1}Sekvenční řešení}{3}%
\contentsline {subsection}{\numberline {1.1}Popis algoritmu sekvenčního řešení}{3}%
\contentsline {subsection}{\numberline {1.2}Výsledky měření}{4}%
\contentsline {section}{\numberline {2}Táskový paralelizmus}{5}%
\contentsline {subsection}{\numberline {2.1}Výsledky měření}{6}%
\contentsline {section}{\numberline {3}Datový paralelizmus}{7}%
\contentsline {subsection}{\numberline {3.1}Výsledky měření}{8}%
\contentsline {section}{\numberline {4}Použití MPI knihoven pro paralelizmus}{9}%
\contentsline {subsection}{\numberline {4.1}Výsledky měření}{10}%
\contentsline {section}{\numberline {5}Časové shrnutí a analýza výsledků}{11}%
\contentsline {section}{\numberline {6}Závěr}{13}%
