#include<stdio.h>
#include<fstream>
#include<iostream>
#include<vector>
#include<cstdint>
#include <bits/stdc++.h>
#include <algorithm>    // std::sort

using namespace std;


class Board{
public:
    Board(short bs): boardSize(bs){
        // create 2D array of pieces
        // store the current position of the movable pieces
        knightCurrentPosition = make_pair(INT_MAX, INT_MAX); // create position with default values
        bishopCurrentPosition = make_pair(INT_MAX, INT_MAX); // create position with default values

        // bishop will be moving first
        currentPieceOnMove = 'S';
    }

    Board(const Board & src){
        boardSize = src.boardSize;
        positionOfPawns = src.positionOfPawns;
        knightCurrentPosition = src.knightCurrentPosition;
        bishopCurrentPosition = src.bishopCurrentPosition;
        currentPieceOnMove = src.currentPieceOnMove;
    }

    /**
     * @brief printBoard function will print the current state of the board
     *  and where are the movable pieces
     *
     */
    void printBoard(){
        for (size_t i = 0; i < boardSize; i++)
        {
            cout << "=";
        } cout << endl;


        cout << "Knight position: " << knightCurrentPosition.first << " " << knightCurrentPosition.second << endl;
        cout << "Bishop position: " << bishopCurrentPosition.first << " " << bishopCurrentPosition.second << endl;

        for (size_t i = 0; i < boardSize; i++)
        {
            cout << "=";
        } cout << endl;


        for (size_t i = 0; i < boardSize; i++){
            for (size_t j = 0; j < boardSize; j++){
            //cout << "(" << i << ", " << j << ")" << endl;
                if(i == knightCurrentPosition.first && j == knightCurrentPosition.second){
                    cout << "J";
                } else if(i == bishopCurrentPosition.first && j == bishopCurrentPosition.second){
                    cout << "S";
                } else if(positionOfPawns.find(make_pair(i, j)) != positionOfPawns.end()){
                    cout << "P";
                }
                else cout << "-";
            }
            cout << endl;
        }
    }

    // remove array of chars and store just the position of the pieces
    short boardSize;
    pair<short, short> knightCurrentPosition;
    pair<short, short> bishopCurrentPosition;
    set<pair<short, short>> positionOfPawns;
    char currentPieceOnMove;
};


/**
 * @brief Function will create next possible moves for the given piece that is on move
 *
 * @param board
 * @return set<pair<short, short>>
 */
set<pair<short, short>> next(const Board & board){
    set<pair<short, short>> avilablePositions;

    // bishop is on move, create set of the available positions and return it
    // fill each available diagonal and remove the diagonal with the knight in the path
    // as the bishop cannot take him out
    if(board.currentPieceOnMove == 'S'){
        // declare local variables for cleaner code
        short x = board.bishopCurrentPosition.first;
        short y = board.bishopCurrentPosition.second;

        // fill right down diagonal
        for (pair<short, short> i = make_pair(x, y); i.first < board.boardSize && i.second < board.boardSize; i = make_pair(i.first+1, i.second+1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill right upper diagonal
        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second < board.boardSize; i = make_pair(i.first-1, i.second+1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill left down diagonal
        for (pair<short, short> i = make_pair(x, y); i.first < board.boardSize && i.second >= 0; i = make_pair(i.first+1, i.second-1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill left upper diagonal
        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second >= 0; i = make_pair(i.first-1, i.second-1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // erase the current position
        avilablePositions.erase(board.bishopCurrentPosition);
    }

    // generate the set of available moves for the knight
    if(board.currentPieceOnMove == 'J'){
        // declare local variables for cleaner code
        short x = board.knightCurrentPosition.first;
        short y = board.knightCurrentPosition.second;

        if(x-1 >= 0 && y-2 >= 0 ) avilablePositions.emplace(make_pair(x-1, y-2));
        if(x-2 >= 0 && y-1 >= 0 ) avilablePositions.emplace(make_pair(x-2, y-1));
        if(x-2 >= 0 && y+1 < board.boardSize) avilablePositions.emplace(make_pair(x-2, y+1));
        if(x-1 >= 0 && y+2 < board.boardSize) avilablePositions.emplace(make_pair(x-1, y+2));
        if(x+1 < board.boardSize && y-2 >= 0) avilablePositions.emplace(make_pair(x+1, y-2));
        if(x+2 < board.boardSize && y-1 >= 0) avilablePositions.emplace(make_pair(x+2, y-1));
        if(x+2 < board.boardSize && y+1 < board.boardSize) avilablePositions.emplace(make_pair(x+2, y+1));
        if(x+1 < board.boardSize && y+2 < board.boardSize) avilablePositions.emplace(make_pair(x+1, y+2));

        // erase position where is bishop
        avilablePositions.erase(board.bishopCurrentPosition);
    }

    // debug
    //for(auto & i : avilablePositions)
    //    cout << "[" << i.first << ", " << i.second << "]" << endl;

    return avilablePositions;
}

/**
 * @brief Check the diagonal after next move so we can search more faster the next positions in recursion
 *
 * @param board
 * @param position
 * @return true return true if on the diagonal we will find the P
 * @return false
 */
bool checkDiagonal(const set<pair<short, short>> positionsOfPawns, short bs, const pair<short,short> position){
        short x = position.first;
        short y = position.second;

        for (pair<short, short> i = make_pair(x, y); i.first < bs && i.second < bs; i = make_pair(i.first+1, i.second+1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second < bs; i = make_pair(i.first-1, i.second+1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first < bs && i.second >= 0; i = make_pair(i.first+1, i.second-1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second >= 0; i = make_pair(i.first-1, i.second-1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        return false;
}

/**
 * @brief Function will evaluate the possible moves in the recursion
 *
 * @param board
 * @param positions
 * @return vector<pair<pair<short,short>, short>>
 */
vector<pair<pair<short,short>, short>> val(const set<pair<short,short>> & positionsOfPawns, const set<pair<short,short>> positions, char pieceOnMove, short bs){
    // pair of coordinates and the assigned value
    vector<pair<pair<short,short>, short>> evaluatedPositions;
    for(auto & i : positions){
        // position contains P as the pesak
        auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
        if(it != positionsOfPawns.end()) evaluatedPositions.emplace_back(make_pair(i, 2));
        // is the bishop on same diagonal?
        else if(pieceOnMove == 'S'){
            if(checkDiagonal(positionsOfPawns, bs, i)) evaluatedPositions.emplace_back(make_pair(i, 1));
        } else evaluatedPositions.emplace_back(make_pair(i, 0));
    }
    return evaluatedPositions;
}

// function to sort vector of the positions and the possible values
bool comparePairsAndValue(const pair<pair<short, short>, short> a, const pair<pair<short, short>, short> b){
    return a.second > b.second;
}

/**
 * @brief recursive function to find the best way how to eliminate the bastards
 *
 * @param board
 * @param turn
 */


int bestFoundValue = 0;
int pawnsCount = 0;
vector<string> result;

void solve(Board & board, int value, int callcount, vector<string> out){
    if(callcount+board.positionOfPawns.size() >= bestFoundValue) return;

    // get all the possible moves
    set<pair<short, short>> possibleMoves = next(board);

    // evaluate next move
    vector<pair<pair<short, short>, short>> moveValue = val(board.positionOfPawns, possibleMoves, board.currentPieceOnMove, board.boardSize);

    // sort it so we search most valued moves first
    sort(moveValue.begin(), moveValue.end(), comparePairsAndValue);

    string toVector;

    for(auto & i : moveValue){
        // create local copy so we dont fuck up original
        if(callcount+board.positionOfPawns.size() >= bestFoundValue) {
            return;
        }
        Board copiedBoard(board);
        pair<short,short> position = i.first;

        auto element = find(copiedBoard.positionOfPawns.begin(), copiedBoard.positionOfPawns.end(), position);
        bool erased = false;
        if(element != copiedBoard.positionOfPawns.end()){
            copiedBoard.positionOfPawns.erase(element);
            if(copiedBoard.positionOfPawns.size() == 0){
                bestFoundValue = min(callcount+1, bestFoundValue);
                toVector = string(1, copiedBoard.currentPieceOnMove) + '(' + to_string(position.first) + ',' + to_string(position.second) + ')' +'*';
                out.emplace_back(toVector);
                result = out;
                return;
            }
            erased = true;
        }

        // change the char on the board
        if(board.currentPieceOnMove == 'S') {
            copiedBoard.bishopCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'J';
            toVector = "S(" + to_string(position.first) + "," + to_string(position.second) + ")";

        } else if(board.currentPieceOnMove == 'J') {
            copiedBoard.knightCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'S';
            toVector = "J(" + to_string(position.first) + "," + to_string(position.second) + ")";
        }
        if(erased) toVector += "* ";
        out.emplace_back(toVector);

        // copiedBoard.printBoard();
        solve(copiedBoard, value + i.second, callcount+1, out);
        out.erase(out.end()-1);
    }
}

int main(int argc, char ** argv){
    short bs;
    char c;

    // get the file in the argument
    ifstream input(argv[1]);

    // read first two lines with board size and max depth and tries
    input >> bs >> bestFoundValue;
    //bestFoundValue = 2*bs*bs;
    Board board(bs);

    for (size_t i = 0; i < bs; i++){
        for(size_t j = 0; j < bs; j++){
            input >> c;

            // create piece and insert it into the array
            if(c == 'P') board.positionOfPawns.emplace(make_pair(i, j));

            // get the position of the movable pieces
            if(c == 'S') board.bishopCurrentPosition = make_pair(i, j);
            if(c == 'J') board.knightCurrentPosition = make_pair(i, j);
        }
    }

    auto start = std::chrono::high_resolution_clock::now();

    // get the most efficient way to delete those pieces
    solve(board, 0, 0, vector<string>());

    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    cout << ((double)duration.count())/1000 << "s" << endl;

    board.printBoard() ;
    cout << bestFoundValue << endl;

    for (auto & i : result) {
        cout << i << " ";
    } cout << endl;

    input.close();

    return 0;
}
