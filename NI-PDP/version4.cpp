#include<stdio.h>
#include<fstream>
#include<iostream>
#include<vector>
#include<cstdlib>
#include<cstdint>
#include <bits/stdc++.h>
#include <algorithm>    // std::sort
#include <mpi.h>

using namespace std;

#define WORKTAG 10
#define QUITTAG 21
#define JOBDONETAG 30

int bestFoundValue = 0;
vector<string> result;

class Board{
public:
    Board(short bs): boardSize(bs){
        // create 2D array of pieces
        // store the current position of the movable pieces
        knightCurrentPosition = make_pair(INT_MAX, INT_MAX); // create position with default values
        bishopCurrentPosition = make_pair(INT_MAX, INT_MAX); // create position with default values

        // bishop will be moving first
        currentPieceOnMove = 'S';
    }

    Board(const Board & src){
        boardSize = src.boardSize;
        positionOfPawns = src.positionOfPawns;
        knightCurrentPosition = src.knightCurrentPosition;
        bishopCurrentPosition = src.bishopCurrentPosition;
        currentPieceOnMove = src.currentPieceOnMove;
    }

    /**
     * @brief printBoard function will print the current state of the board
     *  and where are the movable pieces
     *
     */
    void printBoard(){
        for (size_t i = 0; i < boardSize; i++)
        {
            cout << "=";
        } cout << endl;


        cout << "Knight position: " << knightCurrentPosition.first << " " << knightCurrentPosition.second << endl;
        cout << "Bishop position: " << bishopCurrentPosition.first << " " << bishopCurrentPosition.second << endl;

        for (size_t i = 0; i < boardSize; i++)
        {
            cout << "=";
        } cout << endl;


        for (size_t i = 0; i < boardSize; i++){
            for (size_t j = 0; j < boardSize; j++){
            //cout << "(" << i << ", " << j << ")" << endl;
                if(i == knightCurrentPosition.first && j == knightCurrentPosition.second){
                    cout << "J";
                } else if(i == bishopCurrentPosition.first && j == bishopCurrentPosition.second){
                    cout << "S";
                } else if(positionOfPawns.find(make_pair(i, j)) != positionOfPawns.end()){
                    cout << "P";
                }
                else cout << "-";
            }
            cout << endl;
        }

        cout << "Current on move: " << currentPieceOnMove << endl;

    }

    // remove array of chars and store just the position of the pieces
    short boardSize;
    pair<short, short> knightCurrentPosition;
    pair<short, short> bishopCurrentPosition;
    set<pair<short, short>> positionOfPawns;
    char currentPieceOnMove;
};


/**
 * @brief Function will create next possible moves for the given piece that is on move
 *
 * @param board
 * @return set<pair<short, short>>
 */
set<pair<short, short>> next(const Board & board){
    set<pair<short, short>> avilablePositions;

    // bishop is on move, create set of the available positions and return it
    // fill each available diagonal and remove the diagonal with the knight in the path
    // as the bishop cannot take him out
    if(board.currentPieceOnMove == 'S'){
        // declare local variables for cleaner code
        short x = board.bishopCurrentPosition.first;
        short y = board.bishopCurrentPosition.second;

        // fill right down diagonal
        for (pair<short, short> i = make_pair(x, y); i.first < board.boardSize && i.second < board.boardSize; i = make_pair(i.first+1, i.second+1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill right upper diagonal
        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second < board.boardSize; i = make_pair(i.first-1, i.second+1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill left down diagonal
        for (pair<short, short> i = make_pair(x, y); i.first < board.boardSize && i.second >= 0; i = make_pair(i.first+1, i.second-1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill left upper diagonal
        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second >= 0; i = make_pair(i.first-1, i.second-1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // erase the current position
        avilablePositions.erase(board.bishopCurrentPosition);
    }

    // generate the set of available moves for the knight
    if(board.currentPieceOnMove == 'J'){
        // declare local variables for cleaner code
        short x = board.knightCurrentPosition.first;
        short y = board.knightCurrentPosition.second;

        if(x-1 >= 0 && y-2 >= 0 ) avilablePositions.emplace(make_pair(x-1, y-2));
        if(x-2 >= 0 && y-1 >= 0 ) avilablePositions.emplace(make_pair(x-2, y-1));
        if(x-2 >= 0 && y+1 < board.boardSize) avilablePositions.emplace(make_pair(x-2, y+1));
        if(x-1 >= 0 && y+2 < board.boardSize) avilablePositions.emplace(make_pair(x-1, y+2));
        if(x+1 < board.boardSize && y-2 >= 0) avilablePositions.emplace(make_pair(x+1, y-2));
        if(x+2 < board.boardSize && y-1 >= 0) avilablePositions.emplace(make_pair(x+2, y-1));
        if(x+2 < board.boardSize && y+1 < board.boardSize) avilablePositions.emplace(make_pair(x+2, y+1));
        if(x+1 < board.boardSize && y+2 < board.boardSize) avilablePositions.emplace(make_pair(x+1, y+2));

        // erase position where is bishop
        avilablePositions.erase(board.bishopCurrentPosition);
    }

    // debug
    //for(auto & i : avilablePositions)
    //    cout << "[" << i.first << ", " << i.second << "]" << endl;

    return avilablePositions;
}

/**
 * @brief Check the diagonal after next move so we can search more faster the next positions in recursion
 *
 * @param board
 * @param position
 * @return true return true if on the diagonal we will find the P
 * @return false
 */
bool checkDiagonal(const set<pair<short, short>> positionsOfPawns, short bs, const pair<short,short> position){
        short x = position.first;
        short y = position.second;

        for (pair<short, short> i = make_pair(x, y); i.first < bs && i.second < bs; i = make_pair(i.first+1, i.second+1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second < bs; i = make_pair(i.first-1, i.second+1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first < bs && i.second >= 0; i = make_pair(i.first+1, i.second-1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second >= 0; i = make_pair(i.first-1, i.second-1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        return false;
}

/**
 * @brief Function will evaluate the possible moves in the recursion
 *
 * @param board
 * @param positions
 * @return vector<pair<pair<short,short>, short>>
 */
vector<pair<pair<short,short>, short>> val(const set<pair<short,short>> & positionsOfPawns, const set<pair<short,short>> positions, char pieceOnMove, short bs){
    // pair of coordinates and the assigned value
    vector<pair<pair<short,short>, short>> evaluatedPositions;
    for(auto & i : positions){
        // position contains P as the pesak
        auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
        if(it != positionsOfPawns.end()) evaluatedPositions.emplace_back(make_pair(i, 2));
        // is the bishop on same diagonal?
        else if(pieceOnMove == 'S'){
            if(checkDiagonal(positionsOfPawns, bs, i)) evaluatedPositions.emplace_back(make_pair(i, 1));
        } else evaluatedPositions.emplace_back(make_pair(i, 0));
    }
    return evaluatedPositions;
}

// function to sort vector of the positions and the possible values
bool comparePairsAndValue(const pair<pair<short, short>, short> a, const pair<pair<short, short>, short> b){
    return a.second > b.second;
}
void solveSeq(Board & board, int callcount, vector<string> out){
    if(callcount+board.positionOfPawns.size() >= bestFoundValue) return;

    // get all the possible moves
    set<pair<short, short>> possibleMoves = next(board);

    // evaluate next move
    vector<pair<pair<short, short>, short>> moveValue = val(board.positionOfPawns, possibleMoves, board.currentPieceOnMove, board.boardSize);

    // sort it so we search most valued moves first
    sort(moveValue.begin(), moveValue.end(), comparePairsAndValue);

    string toVector;

    for(auto & i : moveValue){
        // create local copy so we dont fuck up original
        if(callcount+board.positionOfPawns.size() >= bestFoundValue) {
            return;
        }
        Board copiedBoard(board);
        pair<short,short> position = i.first;

        auto element = find(copiedBoard.positionOfPawns.begin(), copiedBoard.positionOfPawns.end(), position);
        bool erased = false;

        if(element != copiedBoard.positionOfPawns.end()){
            copiedBoard.positionOfPawns.erase(element);
            if(copiedBoard.positionOfPawns.size() == 0){

                # pragma omp critical
                    if(callcount+1 < bestFoundValue ) bestFoundValue = callcount+1;
                
                //toVector = string(1, copiedBoard.currentPieceOnMove) + '(' + to_string(position.first) + ',' + to_string(position.second) + ')' +'*';
                string s(1, copiedBoard.currentPieceOnMove);
                out.push_back(s);
                out.push_back("(");
                out.push_back(to_string(position.first));
                out.push_back(",");
                out.push_back(to_string(position.second));
                out.push_back(")");
                out.push_back("*"); 
                //out.emplace_back(toVector);
                
                #pragma omp critical
                    result = out;
               
                return;
            }
            erased = true;
        }

        // change the char on the board
        if(board.currentPieceOnMove == 'S') {
            copiedBoard.bishopCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'J';
            
            // toVector = "S(" + to_string(position.first) + "," + to_string(position.second) + ")";
            //string s(1, copiedBoard.currentPieceOnMove);
            out.push_back("S");
            out.push_back("(");
            out.push_back(to_string(position.first));
            out.push_back(",");
            out.push_back(to_string(position.second));
            out.push_back(")");

        } else if(board.currentPieceOnMove == 'J') {
            copiedBoard.knightCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'S';
            
            //toVector = "J(" + to_string(position.first) + "," + to_string(position.second) + ")";
            out.push_back("J");
            out.push_back("(");
            out.push_back(to_string(position.first));
            out.push_back(",");
            out.push_back(to_string(position.second));
            out.push_back(")");

        }
        if(erased) out.push_back("*");
        //out.emplace_back(toVector);

        // copiedBoard.printBoard();

        // if solution is nearly the best call just sequential
        solveSeq(copiedBoard, callcount+1, out);
        

        if(*(out.end()-1) == "*") out.erase(out.end()-1);
        for (size_t i = 0; i < 6; i++){
            out.erase(out.end()-1);
        } 
        
        //out.erase(out.end()-1);
    }
    #pragma omp taskwai
}

void solve(Board & board, int callcount, vector<string> out){
    if(callcount+board.positionOfPawns.size() >= bestFoundValue) return;

    // get all the possible moves
    set<pair<short, short>> possibleMoves = next(board);

    // evaluate next move
    vector<pair<pair<short, short>, short>> moveValue = val(board.positionOfPawns, possibleMoves, board.currentPieceOnMove, board.boardSize);

    // sort it so we search most valued moves first
    sort(moveValue.begin(), moveValue.end(), comparePairsAndValue);

    string toVector;

    for(auto & i : moveValue){
        // create local copy so we dont fuck up original
        if(callcount+board.positionOfPawns.size() >= bestFoundValue) {
            return;
        }
        Board copiedBoard(board);
        pair<short,short> position = i.first;

        auto element = find(copiedBoard.positionOfPawns.begin(), copiedBoard.positionOfPawns.end(), position);
        bool erased = false;

        if(element != copiedBoard.positionOfPawns.end()){
            copiedBoard.positionOfPawns.erase(element);
            if(copiedBoard.positionOfPawns.size() == 0){

                # pragma omp critical
                    if(callcount+1 < bestFoundValue ) bestFoundValue = callcount+1;
                
                //toVector = string(1, copiedBoard.currentPieceOnMove) + '(' + to_string(position.first) + ',' + to_string(position.second) + ')' +'*';
                string s(1, copiedBoard.currentPieceOnMove);
                out.push_back(s);
                out.push_back("(");
                out.push_back(to_string(position.first));
                out.push_back(",");
                out.push_back(to_string(position.second));
                out.push_back(")");
                out.push_back("*"); 
                //out.emplace_back(toVector);
                
                #pragma omp critical
                    result = out;
               
                return;
            }
            erased = true;
        }

        // change the char on the board
        if(board.currentPieceOnMove == 'S') {
            copiedBoard.bishopCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'J';
            
            // toVector = "S(" + to_string(position.first) + "," + to_string(position.second) + ")";
            //string s(1, copiedBoard.currentPieceOnMove);
            out.push_back("S");
            out.push_back("(");
            out.push_back(to_string(position.first));
            out.push_back(",");
            out.push_back(to_string(position.second));
            out.push_back(")");

        } else if(board.currentPieceOnMove == 'J') {
            copiedBoard.knightCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'S';
            
            //toVector = "J(" + to_string(position.first) + "," + to_string(position.second) + ")";
            out.push_back("J");
            out.push_back("(");
            out.push_back(to_string(position.first));
            out.push_back(",");
            out.push_back(to_string(position.second));
            out.push_back(")");

        }
        if(erased) out.push_back("*");
        //out.emplace_back(toVector);

        // copiedBoard.printBoard();

        // if solution is nearly the best call just sequential
        if(callcount+1 < 8){
            #pragma omp task 
                solve(copiedBoard, callcount+1, out);
        } else {
            solveSeq(copiedBoard, callcount+1, out);
        }

        if(*(out.end()-1) == "*") out.erase(out.end()-1);
        for (size_t i = 0; i < 6; i++){
            out.erase(out.end()-1);
        } 
        
        //out.erase(out.end()-1);
    }
    #pragma omp taskwai
}

class BoardAndState{
public:
    BoardAndState(Board b): boardAndStateBoard(b), callcount(0){};

    BoardAndState(Board b, vector<string> v, int p): boardAndStateBoard(b){
        for(auto & i : v){
            this->previousSteps.push_back(i);
        }
        callcount = 0;
        price = p;
    }

    BoardAndState& operator=(const BoardAndState& other) {
        this->boardAndStateBoard = other.boardAndStateBoard;
        this->callcount = other.callcount;
        this->previousSteps.clear();
        for(auto &i : other.previousSteps)
            this->previousSteps.push_back(i);
        return *this;
    }

    Board boardAndStateBoard;
    vector<string> previousSteps;
    int callcount;
    int price;
};

bool compareTakenPiecesInProblem(BoardAndState a, BoardAndState b){
    return a.price > b.price;
}

queue<BoardAndState> createQueue(Board board, int depth = 0){
 
    // create queue of boards with different states
    queue<BoardAndState> problems;

    // generate possible moves to some given depth 
    // so we van paralelize throught these starting points
    problems.push(BoardAndState(board));

    while (problems.size() < 20){
        // cout << "returning from queue" << endl;    
        // create new possible moves
        BoardAndState tmp = problems.back();
        
        //tmp.boardAndStateBoard.printBoard();

        set<pair<short, short>> possibleMoves = next(tmp.boardAndStateBoard);

        for(auto & i : possibleMoves){
            //cout << i.first << " " << i.second << endl;
            if (problems.size() == 1 ) break;
            BoardAndState a(tmp.boardAndStateBoard, tmp.previousSteps, tmp.price);

            string prevStep;

            // change positions of pieces            
            if(tmp.boardAndStateBoard.currentPieceOnMove == 'S'){
                a.previousSteps.push_back("S");
                a.previousSteps.push_back("(");
                a.previousSteps.push_back(to_string(tmp.boardAndStateBoard.bishopCurrentPosition.first));
                a.previousSteps.push_back(",");
                a.previousSteps.push_back(to_string(tmp.boardAndStateBoard.bishopCurrentPosition.second));
                a.previousSteps.push_back(")");
                auto element = find(a.boardAndStateBoard.positionOfPawns.begin(), a.boardAndStateBoard.positionOfPawns.end(), tmp.boardAndStateBoard.bishopCurrentPosition);
                if(element != a.boardAndStateBoard.positionOfPawns.end()) {
                    a.boardAndStateBoard.positionOfPawns.erase(element);
                    a.previousSteps.push_back("*");
                    a.price += 1;
                }    
                a.boardAndStateBoard.bishopCurrentPosition = i;
                a.boardAndStateBoard.currentPieceOnMove = 'J';
                
            } else {
                a.previousSteps.push_back("J");
                a.previousSteps.push_back("(");
                a.previousSteps.push_back(to_string(tmp.boardAndStateBoard.knightCurrentPosition.first));
                a.previousSteps.push_back(",");
                a.previousSteps.push_back(to_string(tmp.boardAndStateBoard.knightCurrentPosition.second));
                a.previousSteps.push_back(")");
                auto element = find(a.boardAndStateBoard.positionOfPawns.begin(), a.boardAndStateBoard.positionOfPawns.end(), tmp.boardAndStateBoard.knightCurrentPosition);
                if(element != a.boardAndStateBoard.positionOfPawns.end()) {
                    a.boardAndStateBoard.positionOfPawns.erase(element);
                    a.previousSteps.push_back("*");
                    a.price += 1;
                }
                a.boardAndStateBoard.knightCurrentPosition = i;
                a.boardAndStateBoard.currentPieceOnMove = 'S';
            }

            // check if we got pawn on the move
            a.callcount = tmp.callcount +1;

            problems.push(a);

        }
        break;
    }
    // cout << "returning from queue" << endl;    

    vector<BoardAndState> problemsVector;
    int i = 0;
    while(i != problems.size()){         
        problemsVector.push_back(problems.front());
        BoardAndState tmp = problems.front();
        problems.pop();
        problems.push(tmp);
        i++;
    }

    for(auto & it : problemsVector){
        //cout << "Print board from Queue of problems" << endl;
        //it.boardAndStateBoard.printBoard();
        // cout << "=======" << endl;
    }

    return problems;

}

void sendMessageToSlave(int slaveID, BoardAndState & problemToSolve){
    // get and send the board size
    // cout << "===============================" << endl;
    // cout << "Generating and sending message" << endl;
    // cout << "===============================" << endl;
    
    short boardSize = problemToSolve.boardAndStateBoard.boardSize;

    // get knight position and send it
    short knightCurrentPosition[2];
    knightCurrentPosition[0] = problemToSolve.boardAndStateBoard.knightCurrentPosition.first;
    knightCurrentPosition[1] = problemToSolve.boardAndStateBoard.knightCurrentPosition.second;

    // get bishop position  
    short bishopCurrentPosition[2];
    bishopCurrentPosition[0] = problemToSolve.boardAndStateBoard.bishopCurrentPosition.first;
    bishopCurrentPosition[1] = problemToSolve.boardAndStateBoard.bishopCurrentPosition.second;

    // allocate and fill the array of position of pawns
    int pawnCount = problemToSolve.boardAndStateBoard.positionOfPawns.size();
    // short * positionOfPawns = new short[pawnCount*2]();
    short positionOfPawns[40]; for(size_t i = 0; i < 40; i++) positionOfPawns[i] = 30;
    int positionOfPawnIndex = 0; 
    for(auto & it : problemToSolve.boardAndStateBoard.positionOfPawns){                    
        // cout << "positionOFIndex:" << positionOfPawnIndex << endl; 
        // cout << "x:" << it.first << endl; 
        // cout << "x:" << it.second << endl; 
        positionOfPawns[positionOfPawnIndex] = it.first;
        positionOfPawns[positionOfPawnIndex+1] = it.second;
        positionOfPawnIndex += 2;
    }
    
    // get current pieco on move
    char currentPieceOnMove = problemToSolve.boardAndStateBoard.currentPieceOnMove;                

    // get vector of previous steps into the char array
    char previousSteps[100]; for(size_t i = 0; i < 100; i++) previousSteps[i] = 'H';
    for (size_t i = 0; i < problemToSolve.previousSteps.size(); i++){
        //previousSteps[i] = problemToSolve.previousSteps[i];
        strcpy(previousSteps+i, problemToSolve.previousSteps[i].c_str());
    }

    // send callcount
    int callcount = problemToSolve.callcount;

    // get price
    int price = problemToSolve.price;

    // cout << "Sending message to slave: " << slaveID << endl;


    // cout << "Sending message to board size" << boardSize << endl;
    MPI_Send(&boardSize, 1, MPI_SHORT, slaveID, WORKTAG, MPI_COMM_WORLD);
    //cout << "Sending message knight" << knightCurrentPosition[0] << " : " << knightCurrentPosition[1] << endl;
    MPI_Send(&knightCurrentPosition[0], 2, MPI_SHORT, slaveID, WORKTAG, MPI_COMM_WORLD);
    //cout << "Sending message bishop" << bishopCurrentPosition[0] << " : " << bishopCurrentPosition[1] << endl;
    MPI_Send(&bishopCurrentPosition[0], 2, MPI_SHORT, slaveID, WORKTAG, MPI_COMM_WORLD);

    //cout << "position of pawns: ";
    // for (size_t i = 0; i < 40; i++)
    // {
    //     cout << positionOfPawns[i] << " ";
    // } cout << endl;
    MPI_Send(&positionOfPawns[0], 40, MPI_SHORT, slaveID, WORKTAG, MPI_COMM_WORLD);
    
    //cout << "Sending message pieceonmove" << currentPieceOnMove << endl;
    MPI_Send(&currentPieceOnMove, 1, MPI_CHAR, slaveID, WORKTAG, MPI_COMM_WORLD);

    //cout << "Steps: ";
    // for (size_t i = 0; i < 100; i++)
    // {
    //     cout << previousSteps[i] << " ";
    // } cout << endl;     
    MPI_Send(&previousSteps[0], 100, MPI_CHAR, slaveID, WORKTAG, MPI_COMM_WORLD);
   
    //cout << "Sending message callcount" << callcount << endl;
    MPI_Send(&callcount, 1, MPI_INT, slaveID, WORKTAG, MPI_COMM_WORLD);
    
    //cout << "Sending message price" << price << endl;
    MPI_Send(&price, 1, MPI_INT, slaveID, WORKTAG, MPI_COMM_WORLD);


    //cout << "===============================" << endl;
    //cout << "Message sent successfully" << endl;
    //cout << "===============================" << endl;
}

bool compareSolutions(pair<int, char*> a, pair<int, char*> b){
    return a.first < b.first;
}

void sendMessageToMaster(){        
    // cout << "Sending solution to master: " << bestFoundValue << endl;
    MPI_Send(&bestFoundValue, 1, MPI_INT, 0, JOBDONETAG, MPI_COMM_WORLD);
    cout << "Sendeing message: " ;
    char toSend[100];
    int i = 0;
    
    for(auto & it : result){
        cout << result[i] << " ";
        strcpy(toSend+i, result[i].c_str());
        i++;
    } cout << endl;

    MPI_Send(&toSend[0], 100, MPI_CHAR, 0, JOBDONETAG, MPI_COMM_WORLD);
}

int maxRecursion = 0;
int main(int argc, char ** argv){
    short bs;
    char c;
    int commRank, processCount;
    short quit = 21;
    MPI_Status status; 

    // get the file in the argument
    ifstream input(argv[1]);

    // read first two lines with board size and max depth and tries
    input >> bs >> bestFoundValue;
    //bestFoundValue = 2*bs*bs;

    maxRecursion = bestFoundValue;
    Board board(bs);

    for (size_t i = 0; i < bs; i++){
        for(size_t j = 0; j < bs; j++){
            input >> c;

            // create piece and insert it into the array
            if(c == 'P') board.positionOfPawns.emplace(make_pair(i, j));

            // get the position of the movable pieces
            if(c == 'S') board.bishopCurrentPosition = make_pair(i, j);
            if(c == 'J') board.knightCurrentPosition = make_pair(i, j);
        }
    }

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &commRank);

    MPI_Comm_size(MPI_COMM_WORLD, &processCount);

    double t1 = MPI_Wtime();

    // auto start = std::chrono::high_resolution_clock::now();

    if (commRank == 0){
       // generate the moves and add it into queue
        int bestValFromSlave = 999;
        char * resultFromSlave = new char[100];
       
        // cout << "Hello from master" << endl;
        queue<BoardAndState> problems = createQueue(board);

        queue<int> freeSlaves; 
        for(int i = 1; i < processCount; i++) freeSlaves.push(i);

        int workingSlaves=0;
        vector<pair<int, char*>> responseFromSlave;


        while(!problems.empty()){                
            // cout << "Huston, we have problem" << endl;
            BoardAndState problemToSolve = problems.front(); // get the problem to send

            int slaveID = freeSlaves.front(); 

            sendMessageToSlave(slaveID, problemToSolve);
    
            workingSlaves++;

            problems.pop(); // get one problem from the problem queue
            freeSlaves.pop(); // get the slave into the queue again

            // ID in status
            if(freeSlaves.empty()){
                // cout << "Waiting for slaves: " << endl;
                resultFromSlave = new char[100]; 
                // best moves number
                MPI_Recv(&bestFoundValue, 1, MPI_INT, MPI_ANY_SOURCE, JOBDONETAG, MPI_COMM_WORLD, &status);
                // best moves char array 
                MPI_Recv(&resultFromSlave[0], 100, MPI_CHAR, status.MPI_SOURCE, JOBDONETAG, MPI_COMM_WORLD, &status);    
                //waiting to get some free slave
                freeSlaves.push(status.MPI_SOURCE);
                workingSlaves--;

                responseFromSlave.push_back(make_pair(bestFoundValue, resultFromSlave));
            }
        }

        // wait for working slaves
        for (size_t i = 0; i < workingSlaves; i++){
            // cout << i << " " << "Waiting for ws" << endl;
            resultFromSlave = new char[100]; 
            // best moves number
            MPI_Recv(&bestFoundValue, 1, MPI_INT, MPI_ANY_SOURCE, JOBDONETAG, MPI_COMM_WORLD, &status);
            // best moves char array 
            MPI_Recv(&resultFromSlave[0], 100, MPI_CHAR, status.MPI_SOURCE, JOBDONETAG, MPI_COMM_WORLD, &status);    
            //waiting to get some free slave
            freeSlaves.push(status.MPI_SOURCE);

            responseFromSlave.push_back(make_pair(bestFoundValue, resultFromSlave));
        }

        //ending all slaves
        for (int i = 1; i < processCount; ++i){
            short a = 21;
            // cout << "Sending quit to slaves" << endl;
            MPI_Send(&a, 1, MPI_SHORT, i, 1, MPI_COMM_WORLD);
        }

        // get their best results
        sort(responseFromSlave.begin(), responseFromSlave.end(), compareSolutions);

        cout << "Best solution found: " << responseFromSlave.begin()->first << endl;
        for (size_t i = 0; i != (responseFromSlave.begin()->first*6) + board.positionOfPawns.size(); i++){
            cout << responseFromSlave.begin()->second[i] << " ";
        }cout <<endl;
        
        for(auto & it : responseFromSlave){
            delete [] it.second;
        }

    } else {
        while(1){
            short boardSize;
            int price, callcount;
            short knightCurrentPosition[2], bishopCurrentPosition[2];
            short positionOfPawns[40]; for(size_t i = 0; i < 40; i++) positionOfPawns[i] = -1;
            char currentPieceOnMove;
            char previousSteps[100]; for(size_t i = 0; i < 100; i++) previousSteps[i] = 'H';

            MPI_Recv(&boardSize, 1, MPI_SHORT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            // cout << "Got boardsize: " << boardSize << endl;
            if (boardSize == 21){ // send best found result
                // send solution
                // if best found val > 0 and parse vector to char and send again 
                // cout << "Got quittag .. sending solution" << endl;
                break;
            } else { // compute the best result
                // receive the message              
                bestFoundValue = maxRecursion;
                result.clear();

                MPI_Recv(&knightCurrentPosition[0], 2, MPI_SHORT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&bishopCurrentPosition[0], 2, MPI_SHORT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&positionOfPawns[0], 40, MPI_SHORT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&currentPieceOnMove, 1, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&previousSteps[0], 100, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&callcount, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&price, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

                // cout << "Received message" << endl;
                Board board(boardSize);

                // what we got from master

                cout << "Sending message to board size" << boardSize << endl;
                cout << "Sending message knight" << knightCurrentPosition[0] << " : " << knightCurrentPosition[1] << endl;
                cout << "Sending message bishop" << bishopCurrentPosition[0] << " : " << bishopCurrentPosition[1] << endl;

                cout << "position of pawns: ";
                 for (size_t i = 0; i < 40; i++)
                 {
                     cout << positionOfPawns[i] << " ";
                 } cout << endl;

                cout << "Sending message pieceonmove" << currentPieceOnMove << endl;

                cout << "Steps: ";
                 for (size_t i = 0; i < 100; i++)
                 {
                     cout << previousSteps[i] << " ";
                 } cout << endl;     

                cout << "Sending message callcount" << callcount << endl;

                // board.printBoard();

                // get bishop position
                board.bishopCurrentPosition.first = bishopCurrentPosition[0];
                board.bishopCurrentPosition.second = bishopCurrentPosition[1];

                // get knight position
                board.knightCurrentPosition.first = knightCurrentPosition[0];
                board.knightCurrentPosition.second = knightCurrentPosition[1];

                // get current piece on move 
                board.currentPieceOnMove = currentPieceOnMove;

                for (size_t i = 0; positionOfPawns[i] != 30; i+=2){
                    // cout << positionOfPawns[i] << endl;
                    // cout << "i:" << i << endl; 
                    // cout << "x:" << positionOfPawns[i] << endl; 
                    // cout << "x:" << positionOfPawns[i+1] << endl;
                    board.positionOfPawns.emplace(make_pair(positionOfPawns[i], positionOfPawns[i+1]));
                }

                vector<string> output;
                for (size_t i = 0; previousSteps[i] != 'H'; i++){
                    string s(1, previousSteps[i]); // convert char to string
                    output.push_back(s);
                }

//                cout << 

                //board.printBoard();

                #pragma omp parallel num_threads(atoi(argv[2])) 
                {
                    #pragma omp single 
                    {
                        // add variables to pragma as share/private
                        solve(board, callcount, output);
                    }
                }

                // cout << bestFoundValue << endl;

                // for (auto & i : result) {
                //     cout << i << " ";
                // } cout << endl;

                sendMessageToMaster();

            }
        }
    }
    
    /* time measuring - stop */
    double t2 = MPI_Wtime();

    if(commRank == 0)
       printf ("%f.\n",t2-t1);


    MPI_Finalize();

    input.close();

        //auto end = std::chrono::high_resolution_clock::now();
    //auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    //cout << ((double)duration.count())/1000 << "s" << endl;

    return 0;
}
