#include<stdio.h>
#include<fstream>
#include<iostream>
#include<vector>
#include<cstdlib>
#include<cstdint>
#include <bits/stdc++.h>
#include <algorithm>    // std::sort

using namespace std;


class Board{
public:
    Board(short bs): boardSize(bs){
        // create 2D array of pieces
        // store the current position of the movable pieces
        knightCurrentPosition = make_pair(INT_MAX, INT_MAX); // create position with default values
        bishopCurrentPosition = make_pair(INT_MAX, INT_MAX); // create position with default values

        // bishop will be moving first
        currentPieceOnMove = 'S';
    }

    Board(const Board & src){
        boardSize = src.boardSize;
        positionOfPawns = src.positionOfPawns;
        knightCurrentPosition = src.knightCurrentPosition;
        bishopCurrentPosition = src.bishopCurrentPosition;
        currentPieceOnMove = src.currentPieceOnMove;
    }

    /**
     * @brief printBoard function will print the current state of the board
     *  and where are the movable pieces
     *
     */
    void printBoard(){
        for (size_t i = 0; i < boardSize; i++)
        {
            cout << "=";
        } cout << endl;


        cout << "Knight position: " << knightCurrentPosition.first << " " << knightCurrentPosition.second << endl;
        cout << "Bishop position: " << bishopCurrentPosition.first << " " << bishopCurrentPosition.second << endl;

        for (size_t i = 0; i < boardSize; i++)
        {
            cout << "=";
        } cout << endl;


        for (size_t i = 0; i < boardSize; i++){
            for (size_t j = 0; j < boardSize; j++){
            //cout << "(" << i << ", " << j << ")" << endl;
                if(i == knightCurrentPosition.first && j == knightCurrentPosition.second){
                    cout << "J";
                } else if(i == bishopCurrentPosition.first && j == bishopCurrentPosition.second){
                    cout << "S";
                } else if(positionOfPawns.find(make_pair(i, j)) != positionOfPawns.end()){
                    cout << "P";
                }
                else cout << "-";
            }
            cout << endl;
        }

        cout << "Current on move: " << currentPieceOnMove << endl;

    }

    // remove array of chars and store just the position of the pieces
    short boardSize;
    pair<short, short> knightCurrentPosition;
    pair<short, short> bishopCurrentPosition;
    set<pair<short, short>> positionOfPawns;
    char currentPieceOnMove;
};


/**
 * @brief Function will create next possible moves for the given piece that is on move
 *
 * @param board
 * @return set<pair<short, short>>
 */
set<pair<short, short>> next(const Board & board){
    set<pair<short, short>> avilablePositions;

    // bishop is on move, create set of the available positions and return it
    // fill each available diagonal and remove the diagonal with the knight in the path
    // as the bishop cannot take him out
    if(board.currentPieceOnMove == 'S'){
        // declare local variables for cleaner code
        short x = board.bishopCurrentPosition.first;
        short y = board.bishopCurrentPosition.second;

        // fill right down diagonal
        for (pair<short, short> i = make_pair(x, y); i.first < board.boardSize && i.second < board.boardSize; i = make_pair(i.first+1, i.second+1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill right upper diagonal
        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second < board.boardSize; i = make_pair(i.first-1, i.second+1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill left down diagonal
        for (pair<short, short> i = make_pair(x, y); i.first < board.boardSize && i.second >= 0; i = make_pair(i.first+1, i.second-1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // fill left upper diagonal
        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second >= 0; i = make_pair(i.first-1, i.second-1)){
            if(i == board.knightCurrentPosition) break;
            auto it = board.positionOfPawns.find(i);
            if(it != board.positionOfPawns.end());
            avilablePositions.emplace(i);
        }

        // erase the current position
        avilablePositions.erase(board.bishopCurrentPosition);
    }

    // generate the set of available moves for the knight
    if(board.currentPieceOnMove == 'J'){
        // declare local variables for cleaner code
        short x = board.knightCurrentPosition.first;
        short y = board.knightCurrentPosition.second;

        if(x-1 >= 0 && y-2 >= 0 ) avilablePositions.emplace(make_pair(x-1, y-2));
        if(x-2 >= 0 && y-1 >= 0 ) avilablePositions.emplace(make_pair(x-2, y-1));
        if(x-2 >= 0 && y+1 < board.boardSize) avilablePositions.emplace(make_pair(x-2, y+1));
        if(x-1 >= 0 && y+2 < board.boardSize) avilablePositions.emplace(make_pair(x-1, y+2));
        if(x+1 < board.boardSize && y-2 >= 0) avilablePositions.emplace(make_pair(x+1, y-2));
        if(x+2 < board.boardSize && y-1 >= 0) avilablePositions.emplace(make_pair(x+2, y-1));
        if(x+2 < board.boardSize && y+1 < board.boardSize) avilablePositions.emplace(make_pair(x+2, y+1));
        if(x+1 < board.boardSize && y+2 < board.boardSize) avilablePositions.emplace(make_pair(x+1, y+2));

        // erase position where is bishop
        avilablePositions.erase(board.bishopCurrentPosition);
    }

    // debug
    //for(auto & i : avilablePositions)
    //    cout << "[" << i.first << ", " << i.second << "]" << endl;

    return avilablePositions;
}

/**
 * @brief Check the diagonal after next move so we can search more faster the next positions in recursion
 *
 * @param board
 * @param position
 * @return true return true if on the diagonal we will find the P
 * @return false
 */
bool checkDiagonal(const set<pair<short, short>> positionsOfPawns, short bs, const pair<short,short> position){
        short x = position.first;
        short y = position.second;

        for (pair<short, short> i = make_pair(x, y); i.first < bs && i.second < bs; i = make_pair(i.first+1, i.second+1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second < bs; i = make_pair(i.first-1, i.second+1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first < bs && i.second >= 0; i = make_pair(i.first+1, i.second-1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        for (pair<short, short> i = make_pair(x, y); i.first >= 0 && i.second >= 0; i = make_pair(i.first-1, i.second-1)){
            auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
            if(it != positionsOfPawns.end()) return true;
        }

        return false;
}

/**
 * @brief Function will evaluate the possible moves in the recursion
 *
 * @param board
 * @param positions
 * @return vector<pair<pair<short,short>, short>>
 */
vector<pair<pair<short,short>, short>> val(const set<pair<short,short>> & positionsOfPawns, const set<pair<short,short>> positions, char pieceOnMove, short bs){
    // pair of coordinates and the assigned value
    vector<pair<pair<short,short>, short>> evaluatedPositions;
    for(auto & i : positions){
        // position contains P as the pesak
        auto it = find(positionsOfPawns.begin(), positionsOfPawns.end(), i);
        if(it != positionsOfPawns.end()) evaluatedPositions.emplace_back(make_pair(i, 2));
        // is the bishop on same diagonal?
        else if(pieceOnMove == 'S'){
            if(checkDiagonal(positionsOfPawns, bs, i)) evaluatedPositions.emplace_back(make_pair(i, 1));
        } else evaluatedPositions.emplace_back(make_pair(i, 0));
    }
    return evaluatedPositions;
}

// function to sort vector of the positions and the possible values
bool comparePairsAndValue(const pair<pair<short, short>, short> a, const pair<pair<short, short>, short> b){
    return a.second > b.second;
}

/**
 * @brief recursive function to find the best way how to eliminate the bastards
 *
 * @param board
 * @param turn
 */


int bestFoundValue = 0;
vector<string> result;

void solve(Board & board, int callcount, vector<string> out){
    if(callcount+board.positionOfPawns.size() >= bestFoundValue) return;

    // get all the possible moves
    set<pair<short, short>> possibleMoves = next(board);

    // evaluate next move
    vector<pair<pair<short, short>, short>> moveValue = val(board.positionOfPawns, possibleMoves, board.currentPieceOnMove, board.boardSize);

    // sort it so we search most valued moves first
    sort(moveValue.begin(), moveValue.end(), comparePairsAndValue);

    string toVector;

    for(auto & i : moveValue){
        // create local copy so we dont fuck up original
        if(callcount+board.positionOfPawns.size() >= bestFoundValue) {
            return;
        }
        Board copiedBoard(board);
        pair<short,short> position = i.first;

        auto element = find(copiedBoard.positionOfPawns.begin(), copiedBoard.positionOfPawns.end(), position);
        bool erased = false;

        if(element != copiedBoard.positionOfPawns.end()){
            copiedBoard.positionOfPawns.erase(element);
            if(copiedBoard.positionOfPawns.size() == 0){

                # pragma omp critical
                    if(callcount+1 < bestFoundValue ) bestFoundValue = callcount+1;
                
                toVector = string(1, copiedBoard.currentPieceOnMove) + '(' + to_string(position.first) + ',' + to_string(position.second) + ')' +'*';
                out.emplace_back(toVector);
                
                #pragma omp critical
                    result = out;
               
                return;
            }
            erased = true;
        }

        // change the char on the board
        if(board.currentPieceOnMove == 'S') {
            copiedBoard.bishopCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'J';
            toVector = "S(" + to_string(position.first) + "," + to_string(position.second) + ")";

        } else if(board.currentPieceOnMove == 'J') {
            copiedBoard.knightCurrentPosition = position;
            copiedBoard.currentPieceOnMove = 'S';
            toVector = "J(" + to_string(position.first) + "," + to_string(position.second) + ")";
        }
        if(erased) toVector += "* ";
        out.emplace_back(toVector);

        // copiedBoard.printBoard();
        solve(copiedBoard, callcount+1, out);
        out.erase(out.end()-1);
    }
}


class BoardAndState{
public:
    BoardAndState(Board b): boardAndStateBoard(b), callcount(0){};

    BoardAndState(Board b, vector<string> v, int p): boardAndStateBoard(b){
        for(auto & i : v){
            this->previousSteps.push_back(i);
        }
        callcount = 0;
        price = p;
    }

    BoardAndState& operator=(const BoardAndState& other) {
        this->boardAndStateBoard = other.boardAndStateBoard;
        this->callcount = other.callcount;
        this->previousSteps.clear();
        for(auto &i : other.previousSteps)
            this->previousSteps.push_back(i);
        return *this;
    }

    Board boardAndStateBoard;
    vector<string> previousSteps;
    int callcount;
    int price;
};

bool compareTakenPiecesInProblem(BoardAndState a, BoardAndState b){
    return a.price > b.price;
}

void createQueue(Board board, int threads){
    
    // create queue of boards with different states
    queue<BoardAndState> problems;

    // generate possible moves to some given depth 
    // so we van paralelize throught these starting points
    problems.push(BoardAndState(board));

    while (problems.size() < 20){
        // create new possible moves
        BoardAndState tmp = problems.back();
        
        //tmp.boardAndStateBoard.printBoard();

        set<pair<short, short>> possibleMoves = next(tmp.boardAndStateBoard);

        for(auto & i : possibleMoves){
            //cout << i.first << " " << i.second << endl;
            BoardAndState a(tmp.boardAndStateBoard, tmp.previousSteps, tmp.price);

            string prevStep;

            // change positions of pieces            
            if(tmp.boardAndStateBoard.currentPieceOnMove == 'S'){
                prevStep = "S(" + to_string(tmp.boardAndStateBoard.bishopCurrentPosition.first) + "," + to_string(tmp.boardAndStateBoard.bishopCurrentPosition.second) + ")";
                a.previousSteps.push_back(prevStep);
                auto element = find(a.boardAndStateBoard.positionOfPawns.begin(), a.boardAndStateBoard.positionOfPawns.end(), tmp.boardAndStateBoard.bishopCurrentPosition);
                if(element != a.boardAndStateBoard.positionOfPawns.end()) {
                    a.boardAndStateBoard.positionOfPawns.erase(element);
                    a.previousSteps.push_back("*");
                    a.price += 1;
                }    
                a.boardAndStateBoard.bishopCurrentPosition = i;
                a.boardAndStateBoard.currentPieceOnMove = 'J';
                
            } else {
                prevStep = "J(" + to_string(tmp.boardAndStateBoard.knightCurrentPosition.first) + "," + to_string(tmp.boardAndStateBoard.knightCurrentPosition.second) + ")";
                a.previousSteps.push_back(prevStep);
                auto element = find(a.boardAndStateBoard.positionOfPawns.begin(), a.boardAndStateBoard.positionOfPawns.end(), tmp.boardAndStateBoard.knightCurrentPosition);
                if(element != a.boardAndStateBoard.positionOfPawns.end()) {
                    a.boardAndStateBoard.positionOfPawns.erase(element);
                    a.previousSteps.push_back("*");
                    a.price += 1;
                }
                a.boardAndStateBoard.knightCurrentPosition = i;
                a.boardAndStateBoard.currentPieceOnMove = 'S';
            }

            // check if we got pawn on the move
            a.callcount = tmp.callcount +1;

            //cout << "Pushing: " << endl;
            //a.boardAndStateBoard.printBoard();
            //cout << a.callcount << endl;
            //for(auto & j : a.previousSteps){
            //    cout << j;
            //} cout << endl;

            problems.push(a);

        }
               
    }
    
    // get data from Queue into the Vector for better parallel computing
    vector<BoardAndState> problemsVector;
    while(!problems.empty()){         
        problemsVector.push_back(problems.front());
        problems.pop();
    }


    sort(problemsVector.begin(), problemsVector.end(), compareTakenPiecesInProblem);

    // Solve the queue sequential with the normal function
    # pragma omp parallel for num_threads(threads)
        for(unsigned int i = 0; i < problemsVector.size(); i++){
            //cout << "Solving: " << endl;
            solve(problemsVector[i].boardAndStateBoard, problemsVector[i].callcount, problemsVector[i].previousSteps);
        }
}

int main(int argc, char ** argv){
    short bs;
    char c;

    // get the file in the argument
    ifstream input(argv[1]);
    
    // read first two lines with board size and max depth and tries
    input >> bs >> bestFoundValue;
    //bestFoundValue = 2*bs*bs;
    Board board(bs);

    for (size_t i = 0; i < bs; i++){
        for(size_t j = 0; j < bs; j++){
            input >> c;

            // create piece and insert it into the array
            if(c == 'P') board.positionOfPawns.emplace(make_pair(i, j));

            // get the position of the movable pieces
            if(c == 'S') board.bishopCurrentPosition = make_pair(i, j);
            if(c == 'J') board.knightCurrentPosition = make_pair(i, j);
        }
    }

    // get the most efficient way to delete those pieces
    // solve(board, 0, 0, vector<string>());

    auto start = std::chrono::high_resolution_clock::now();

    createQueue(board, atoi(argv[2]));

    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    cout << ((double)duration.count())/1000 << "s" << endl;


    //board.printBoard() ;
    cout << bestFoundValue << endl;

    for (auto & i : result) {
        cout << i << " ";
    } cout << endl;

    input.close();

    return 0;
}
