# Systemd unit file

# Script

Sluzba se spousti pres script, ktery spousti unit file:

```
#!/bin/bash

docker run -d --net=host app
```

# Unit File

Soubor musi byt na ceste: /etc/systemd/system/<file>

```
[Unit]
Description=Python app

[Service]
ExecStart=/opt/run-app.sh

ProtectSystem=strict
ProtectKernelTunables=true
ProtectControlGroups=true
ProtectKernelModules=true

[Install]
WantedBy=multi-user.target
```

# Result

→ Overall exposure level for app.service: 8.5 EXPOSED 🙁
