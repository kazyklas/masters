# Images

Obrazy jsem zvolil ze sve bakalarske prace.

1. manjaro/base
2. hashcatwrapper (vcc-test)

Z pulnuteho image (manjaro/base) jsem slozil vcc-test.
Tyto obrazy jsem nasledne pomoci `docker save` ulozil a nasledne rozbalil a porovnal v cem se lisi. 
Vystup je v souboru diff-images.txt. 

# Dockerfile

Kazdy prikaz v Dockerfile vytvari novou vrstvu. Pokusim se tedy namapovat vrstvy na prikazy:

```
COPY main.py /opt/

4422074b1a7f5fd804f84af9aec6ce22c0fce10bf0d5073f97393997a63fab8c
├── json
├── layer.tar
├── layer.tar.unzip
│   └── opt
│       └── main.py
└── VERSION
```

```
COPY install.sh /opt/

6ec81e2b49a188c490c4cf9fefb289a1899075041b4a5e0f4ccae8440a2d85b2
├── json
├── layer.tar
├── layer.tar.unzip
│   └── opt
│       └── install.sh
└── VERSION
```


```
OPY dict.txt /opt

76c6bc5058e3e961c5c6fd2669e4febce98934fef22f74f341622d8dcf6e60cb
├── json
├── layer.tar
├── layer.tar.unzip
│   └── opt
│       └── dict.txt
└── VERSION
```

```
RUN chmod +x /opt/install.sh

b8fe40b4df5984a010af5464cff3c744ca8ff0d036f3f514cd5d8c109dbeebe8
├── json
├── layer.tar
├── layer.tar.unzip
│   └── opt
│       └── install.sh
└── VERSION
```

```
RUN /opt/install.sh

c430a130c995bb30c5df799034c5ec1d2bdc0a7bf0426b7e6e807e1ff3bdf216
├── json
├── layer.tar
├── layer.tar.unzip
│   ├── etc
│   │   ├── audit
│   │   ├── avahi
│   │   ├── bash.bash_logout
│   │   ├── bash.bashrc
│   │   ├── bindresvport.blacklist
│   │   ├── ca-certificates
│   │   ├── conf.d
│   │   ├── dbus-1
│   │   ├── dconf
│   │   ├── e2scrub.conf
│   │   ├── fonts
│   │   ├── gai.conf
│   │   ├── group
│   │   ├── group-
│   │   ├── gshadow
│   │   ├── gshadow-
│   │   ├── gtk-3.0
│   │   ├── healthd.conf
│   │   ├── inputrc
│   │   ├── iproute2
│   │   ├── krb5.conf
│   │   ├── ld.so.cache
│   │   ├── ld.so.conf.d
│   │   ├── libaudit.conf
│   │   ├── libnl
│   │   ├── locale.gen.pacnew
│   │   ├── makepkg.conf
│   │   ├── mke2fs.conf
│   │   ├── mtab -> ../proc/self/mounts
│   │   ├── netconfig
│   │   ├── nscd.conf
│   │   ├── OpenCL
│   │   ├── openldap
│   │   ├── pacman.conf
│   │   ├── pacman.d
│   │   ├── pacman-mirrors.conf
│   │   ├── pamac.conf
│   │   ├── pam.d
│   │   ├── passwd
│   │   ├── passwd-
│   │   ├── pinentry
│   │   ├── profile.d
│   │   ├── rpc
│   │   ├── sensors3.conf
│   │   ├── shadow
│   │   ├── shadow-
│   │   ├── skel
│   │   ├── ssl
│   │   ├── sudo.conf
│   │   ├── sudoers
│   │   ├── sudoers.d
│   │   ├── sudo_logsrvd.conf
│   │   ├── systemd
│   │   ├── udev
│   │   ├── X11
│   │   ├── xattr.conf
│   │   ├── xdg
│   │   └── xinetd.d
│   ├── home
│   │   └── notroot
│   ├── opt
│   │   └── intel
│   ├── run
│   │   ├── faillock
│   │   ├── lock
│   │   ├── log
│   │   ├── nscd
│   │   ├── sudo
│   │   ├── systemd
│   │   └── user
│   ├── tmp
│   ├── usr
│   │   ├── bin
│   │   ├── include
│   │   ├── lib
│   │   ├── lib32
│   │   └── share
│   └── var
│       ├── cache
│       ├── db
│       ├── lib
│       ├── log
│       └── tmp
└── VERSION
```
