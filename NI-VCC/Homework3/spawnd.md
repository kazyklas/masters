# Systemd-nspawn

Rozhodl jsem se jako fanda Arch linux based distribuci rozjet arch systemovy kontejner.

## Postup 

```
# Instalace balicku na ubuntu
apt install systemd-container

# Stazeni systemu do /var/lib/machines
machinectl pull-tar --verify=no https://archive.archlinux.org/iso/2020.04.01/archlinux-bootstrap-2020.04.01-x86_64.tar.gz arch

# Issue se spatnymi adresari po stazeni jsem vyresil takto:
cp /var/lib/machines/arch/root.x86_64/* /var/lib/machines/arch2

# Spusteni kontejneru
systemd-nspawn -M arch2
```