#!/bin/sh

useradd -m notroot

pacman -Sy --noconfirm archlinux-keyring && \
pacman -Sy --noconfirm base-devel git python-pip hashcat && \
pacman -Syu --noconfirm

# Allow notroot to run stuff as root (to install dependencies):
echo "notroot ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/notroot
cd /home/notroot

whoami
sudo -i -u notroot << EOF
whoami
# install ncurses
git clone https://aur.archlinux.org/ncurses5-compat-libs.git
gpg --recv-key 702353e0f7e48edb
cd ncurses5-compat-libs
makepkg --noconfirm --syncdeps --rmdeps --install --clean
cd ..

# install intel opencl
git clone https://aur.archlinux.org/intel-opencl-runtime.git
cd intel-opencl-runtime
makepkg --noconfirm --syncdeps --rmdeps --install --clean
cd ..

rm -rf intel-opencl-runtime ncurses5-compat-libs

pip install requests 

EOF

pacman -R --noconfirm base-devel archlinux-keyring 

rm -rf /var/cache/pacman/pkg/
