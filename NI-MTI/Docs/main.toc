\selectlanguage *{latex}
\contentsline {section}{\numberline {1}VoIP}{4}%
\contentsline {subsection}{\numberline {1.1}Callable from the outside of the network}{5}%
\contentsline {subsection}{\numberline {1.2}Old technology support}{5}%
\contentsline {subsection}{\numberline {1.3}Codecs}{5}%
\contentsline {section}{\numberline {2}VLANs}{6}%
\contentsline {subsection}{\numberline {2.1}User authentication and VLAN assign}{7}%
\contentsline {subsection}{\numberline {2.2}Voice VLAN}{7}%
\contentsline {subsubsection}{\numberline {2.2.1}Old VLAN configuration}{7}%
\contentsline {section}{\numberline {3}Redundancy in the design}{8}%
\contentsline {subsection}{\numberline {3.1}Redundancy in distribution network part}{8}%
\contentsline {subsection}{\numberline {3.2}Datalink redundancy}{8}%
\contentsline {subsection}{\numberline {3.3}Router redundancy}{8}%
\contentsline {subsection}{\numberline {3.4}DNS redundancy}{9}%
\contentsline {section}{\numberline {4}Prioritize the VoIP data transfer}{10}%
\contentsline {subsection}{\numberline {4.1}QoS}{10}%
\contentsline {subsection}{\numberline {4.2}Priority queue}{11}%
\contentsline {subsection}{\numberline {4.3}VoIP standards}{11}%
\contentsline {subsection}{\numberline {4.4}TCP resonance}{12}%
\contentsline {section}{\numberline {5}Modernize VPN in scalable way}{13}%
\contentsline {subsection}{\numberline {5.1}IPsec}{13}%
\contentsline {subsection}{\numberline {5.2}Redundancy in VPN}{13}%
\contentsline {subsection}{\numberline {5.3}Connect users from outside}{14}%
\contentsline {section}{\numberline {6}Services available from WAN}{14}%
\contentsline {section}{\numberline {7}Security}{15}%
\contentsline {subsection}{\numberline {7.1}Firewall}{15}%
\contentsline {subsection}{\numberline {7.2}Proxy servers}{15}%
\contentsline {subsection}{\numberline {7.3}Max MAC addresses on fastEthernet}{15}%
\contentsline {subsection}{\numberline {7.4}DHCP snooping}{15}%
\contentsline {subsection}{\numberline {7.5}Best effort class changing}{16}%
\contentsline {section}{\numberline {8}Network diagram}{17}%
\contentsline {section}{\numberline {9}Summary}{18}%
\contentsline {section}{\numberline {10}Used shortcuts}{19}%
