import numpy as np
import click

# seterr to raise errors and not warning on overflows
np.seterr(all='raise')

def gauss_seidel(matrix, b, x, accuracy=10 ** -6):
    configurations = 0
    upper_triangular_strict = np.triu(matrix, 1)
    lower_triangular = np.tril(matrix)

    while True:
        configurations += 1

        try:
            x = np.linalg.inv(lower_triangular) @ (b - (upper_triangular_strict @ x))
            if np.linalg.norm(matrix @ x - b) / np.linalg.norm(b) < accuracy:
                break
        except FloatingPointError:
            return configurations, False

    return configurations, True


# Links to documentation
# https://numpy.org/doc/stable/reference/generated/numpy.matrix.fill.html
# https://numpy.org/doc/stable/reference/generated/numpy.fill_diagonal.html
def initializeProblem(gamma):
    # fill the matrix
    toReturnMatrix = np.zeros((20, 20), np.double)
    np.fill_diagonal(toReturnMatrix[1:], -1)
    np.fill_diagonal(toReturnMatrix[:, 1:], -1)
    np.fill_diagonal(toReturnMatrix, gamma)

    # create b vector
    bVectorToReturn = np.full((20, 1), gamma - 2, np.double)
    bVectorToReturn[0] = gamma - 1
    bVectorToReturn[-1] = gamma - 1

    # create x vector
    xVectorToReturn = np.zeros((20, 1))
    
    return toReturnMatrix, bVectorToReturn, xVectorToReturn


def main():
    for i in (1, 2, 3):
        matrix, b, x = initializeProblem(i)
        configurations, result = gauss_seidel(matrix, b, x)
        print("Gamma: ", i)
        if result:
            print("Number of configurations: ", configurations)
        else:
            print("Overflow after: ", configurations)



if __name__ == '__main__':
    main()
