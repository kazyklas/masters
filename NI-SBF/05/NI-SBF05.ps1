﻿$Items = @('Archived History', <# Veci, ktere budu chtit, aby se smazaly #>
            'Cache\*',
            'Cookies',
            'History',
            'Login Data',
            'Top Sites',
            'Visited Links',
            'Web Data')

$ChromeArtefacts = "$($env:LOCALAPPDATA)\Google\Chrome\User Data\Default" <# Cesta, kde google chrome uklada sva data #>

$RecentItems = "$($env:APPDATA)\Microsoft\Windows\Recent" <# jump list a recent files #>

$Items | % {     
    if (Test-Path "$ChromeArtefacts\$_") { 
           Remove-Item "$ChromeArtefacts\$_"  
           }
 }

Remove-Item -Path $RecentItems -Recurse -Force