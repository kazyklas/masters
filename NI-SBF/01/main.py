import requests
import glob
import time

def scan(filename):
    url = 'https://www.virustotal.com/vtapi/v2/file/scan'
    params = {'apikey': '27c3e2ba36289463aedfc9c54458f948b0ec6fa860a3ffb74761c5e516546735'}
    files = {'file': (filename, open(filename, 'rb'))}
    response = requests.post(url, files=files, params=params)
    print(response.json())
    return response.json()

def getResponse(resource):
    url = 'https://www.virustotal.com/vtapi/v2/file/report'
    params = {'apikey': '27c3e2ba36289463aedfc9c54458f948b0ec6fa860a3ffb74761c5e516546735', 'resource': resource}
    while True:
        response = requests.get(url, params=params)
        if response.status_code != 200:
            print("waitin\' until I can request the reports again...")
            time.sleep(9)
        else:
            break
    print(response.json())
    return response.json()

if __name__ == '__main__':
    sentFiles = []
    filesToScan = glob.glob("C:/Users/Martin/Documents/suspicious 2/suspicious/*")
    for element in filesToScan:
        sentFiles.append(scan(element))
    print("----------------------------")
    sentResponses = []
    for sentFile in sentFiles:
        sentResponses.append(getResponse(sentFile["md5"]))
    for fileName in filesToScan:
        resp = sentResponses.pop(0)
        print(fileName + "\t|\tPocet provedenych testu: " + str(resp["total"]) + "\t|\tPocet pozitivnich detekci: " + str(resp["positives"]))



