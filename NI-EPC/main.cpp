#include<iostream>
#include<thread>
#include<vector>


int main() {
    std::vector<int> v = {1, 2, 3, 4, 5, 6};
    int count = 0;    
    int limit = 0;
    std::cout << " Hello world" << std::endl;
    return 0;
}

/*

   Lambda function capture 
#include<iostream>
#include<thread>
#include<vector>


int main() {
    std::vector<int> v = {8, 2, 9, 1, 2, 3, 4, 5, 6}; 
    //std::vector<int> v = {8, 0, 1, 0, 0, 0, 0, 1, 1}; 
    int count = 0;    
    int limit = 5;
    std::cin >> limit;
    // if v.element < limit v.element = false;
    // else v.element = true; count ++;
    
    // bool cmp(const Type1 &a, const Type2 &b);
    // true if the first argument is less than (i.e. is ordered before) the second. 
    
    bool first = true;
    std::sort(std::begin(v), std::end(v), [&first = true, &limit, &count](int& a, int& b){
        //std::cout << "a = " << a << ", b = " << b << std::endl;
        if(a < limit) {
            a = 0;
        } else {
           if (a > limit) 
               count ++;
           a = 1;
        }
        if (first) {
            if(b < limit) {
                b = 0;
            } else {
               if (b > limit) 
                   count ++;
               b = 1;
            }
            first = false;
        }
        return false;
    });

    //if(a < limit) a = 0; else { count++, a = true; }; if()

    std::cout << count << std::endl;
    for(const auto &i : v)
        std::cout << i << ", ";
}


# casts and polymorfismus

#include<iostream>
#include<thread>
#include<vector>

template<typename T>
class BetterVector: public std::vector<T> {
public:
    void release() {
        std::vector<T>::clear();
        std::vector<T>::shrink_to_fit();
    }
};

int main(){
    using int_vec_t = std::vector<int>;
    std::unique_ptr<int_vec_t> upvi(new BetterVector<int>());
    upvi->resize(1000, -1);
    auto bvi = static_cast<BetterVector<int>*>(upvi.get());
    bvi->release();
}


c-style:
short foo = 3;
int i = (int)foo;

static-cast
    // static_cast performs no runtime checks.
    void* x = ...
    MyClass* my = static_cast<MyClass*>(x);
    The static_cast operator performs a nonpolymorphic cast. For example, it can be used to cast a base class pointer into a derived class pointer.

    short -> int
    unsigned int -> short

reinterpret_cast
        umi vsechno a vsechno rozmrda pokud to spatne pouzivjeme na tridach

dynamic_cast
        kouka na bloky a na prdky a kontroluje tridy do jakych patri = je drahej
        The dynamic_cast performs a runtime cast that verifies the validity of the cast.
        A dynamic_cast performs casts on polymorphic types and can cast a A* pointer into a B* pointer only if the object being pointed to actually is a B object.

const_cast
        casting to non/const


# Mutexes and threads 

        #include<iostream>
#include<thread>
#include<vector>


int main()
{
    int i = 0;
    std::mutex m;

    std::thread t([&i, &m]() {
        m.lock();
        i++;
        m.unlock();
    });

    m.lock();
    i++;
    m.unlock();

    t.join();

    std::cout << i << std::endl;

    return 0;
}

# Scope guard

#include<iostream>
#include<thread>
#include<vector>


int main()
{
    int i = 0;
    std::mutex m;

    std::thread t([&i, &m]() {
        std::lock_guard<std::mutex> lock(m);
        i++;
    });

    {
        std::lock_guard<std::mutex> lock(m);
        i++;
    }

    t.join();

    std::cout << i << std::endl;

    return 0;
}

# type traits 



#include<iostream>
#include<thread>
#include<vector>
#include<cassert>
#include<set>
#include <list>
#include <type_traits>


template <typename T>
struct is_set
{
    static constexpr auto value = std::is_base_of<std::set<typename T::value_type>, T>::value;
};


int main()
{
   assert(
       is_set<   std::set<int>    >::value
           == true);

   assert(
         is_set<   std::list<float>   >::value
           == false);

   assert(
         is_set<   int   >::value
           == false);


}




*/

/* 

#include<iostream>
#include<thread>
#include<vector>
#include<cassert>
#include<set>
#include <list>
#include <type_traits>

/*
template <typename T>
struct is_set
{
    static constexpr auto value = std::is_base_of<std::set<typename T::value_type>, T>::value;
};
*/



