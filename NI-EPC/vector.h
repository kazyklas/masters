#ifndef EPC_SMALL_VECTOR
#define EPC_SMALL_VECTOR

#include <cstdlib>
#include <initializer_list>
#include <memory>
#include <iostream>


namespace epc {
    template <class T, size_t N = 8>
    class small_vector {
    public:
        using value_type = T; // TODO rewrite to something else
        
        small_vector()
            : data_(nullptr), capacity_(N), size_(0){}
         
        small_vector(std::initializer_list<T> l){
            // If the size of the list is smaller than static array
            //      add it into the allocated space in the compilation time
            if(l.size() < N){
                try { 
                    std::uninitialized_copy(l.begin(), l.end(), data());  
                }
                catch (...) { 
                    ::operator delete(data()); 
                    throw; 
                }
                size_ = l.size();
                capacity_ = N;
                data_ = nullptr;
            }

            // else allocate the space dynamically 
            else{
                data_ = (T*)::operator new(capacity_ * l.size());
                capacity_ = l.size();
                size_ = l.size();
                try { 
                    std::uninitialized_copy(l.begin(), l.end(), data_);  
                }
                catch (...) { 
                    ::operator delete(data_); 
                    throw; 
                }
            }
        }
 
        /**
         * @brief Construct a new small vector object, copy constructor
         * 
         * @param other 
         */
        small_vector(const small_vector& other) : small_vector() {
            if (other.size_ == 0) return;
            
            // can we fit that stuff into the buffer?
            if (other.size_ <= N){
                for(size_t i = 0; i < other.size_; i++)
                    new (data() + i) T(other.data()[i]);

                // allocate the data_ based on capacity
            } else { // no we can't
                data_ = (T*)::operator new(other.size_ * sizeof(T));
                capacity_ = other.size_;
                try {
                    std::uninitialized_copy(other.data(), other.data() + other.size_, data_);
                } catch(...) { ::operator delete(data_); throw; }
            }
            
            size_ = other.size_;
        }

        /**
         * @brief Construct a new small vector object, move contructor
         * 
         * @param other 
         */
        small_vector(small_vector&& other) : small_vector() {
            if (other.size_ == 0) return;

            // can we fit that stuff into the buffer?
            if (other.size_ <= N){
                for(size_t i = 0; i < other.size_; i++)
                    new (data() + i) T(std::move_if_noexcept(other.data()[i]));
            } else { // no we can't
                data_ = (T*)::operator new(other.size_ * sizeof(T));
                capacity_ = other.size_;
                try {
                    std::uninitialized_move(other.data(), other.data() + other.size_, data_);
                } catch(...) { ::operator delete(data_); throw; }
            }
            
            size_ = other.size_;

            other.clear();
            other.size_ = 0;
        }

        /**
         * @brief operator prirazeni 
         * 
         * @param other  
         * @return small_vector& 
         */
        small_vector& operator=(const small_vector& other) {
            if (capacity_ < other.size_) { // if capacity is not sufficient...
                // ...copy vector by copy-and-swap idiom...
                small_vector temp(other); 
                swap(temp);
            } else { // ...otherwise, use allocated memory
                if (this != &other) {
                    clear();
                    reserve(other.size_);
                    std::uninitialized_copy(other.data(), other.data() + other.size_, data());
                    size_ = other.size_;
                }
            }
            return *this;
        }

        /**
         * @brief move operator
         * 
         * @param other 
         * @return small_vector& 
         */
        small_vector& operator=(small_vector&& other) noexcept { // move-by-swap
            swap(other); 
            other.clear(); 
            return *this;
        }

        /**
         * @brief This function will swap the content of two vectors
         *  swap function is used in the copy-swap idiom
         * @param other Another vector
         */
        void swap(small_vector& other) noexcept {
            if(other.capacity_ <= N && capacity_ <= N){ // both vectors are small
                // we need to switch the elements in the buffer
                if(other.size_ == size_) // same size in buffer
                    for(size_t i = 0; i < size_; i++)
                        std::swap(data()[i], other.data()[i]);
                else if(other.size_ > size_){ // other is bigger
                    for(size_t i = 0; i < size_; i++) // swap and then copy and delete
                        std::swap(data()[i], other.data()[i]); 
                    for(size_t i = size_; i < other.size_; i++){
                        new (data()+i) T(std::move_if_noexcept(other.data()[i]));
                        other.data()[i].~T();
                    }
                } else {
                    for(size_t i = 0; i < other.size_; i++) // swap and then copy and delete 
                        std::swap(data()[i], other.data()[i]);
                    for(size_t i = other.size_; i < size_; i++){
                        new (other.data()+i) T(std::move_if_noexcept(data()[i]));
                        other.data()[i].~T();
                    }
                }
            } 
            else if(other.capacity_ > N && capacity_ <= N) { // one small and other is big
                // backup pointer for ensuring buffer copy
                T * temp = other.data_;
                other.data_ = nullptr;

                // buffer swap 
                for (size_t i = 0; i < capacity_; i++){
                    new (other.data() + i) T(std::move_if_noexcept(data()[i])); 
                    data()[i].~T();
                }
                
                // switch pointers to right data
                data_ = temp; 
            } else if(other.capacity_ <= N && capacity_ > N){ // one small and other is big
                //std::cout << "The called is bigger" << std::endl;
                T * temp = data_;
                data_ = nullptr;
                
                // buffer swap
                for (size_t i = 0; i < other.capacity_; i++){
                    new (data() + i) T(std::move_if_noexcept(other.data()[i]));
                    data()[i].~T();
                }

                // swap the pointers
                other.data_ = temp;
            } else { // both vectors are big => swap just pointers to data 
                std::swap(data_, other.data_);
            }

            // swap capacity and size
            std::swap(capacity_, other.capacity_);
            std::swap(size_, other.size_);
        }

        /**
         * @brief Return the element on the position in the vector
         * 
         * @param i the position
         * @return T& 
         */
        T& operator[](size_t i){ return data()[i]; }
        const T& operator[](size_t i) const { return data()[i]; }
 
        /**
         * @brief Helping function for destructor and memory free
         * 
         */
        void clear() noexcept{ 
            try {
                if(data())
                    for ( ; size_ > 0; size_--) 
                        (data()[size_-1]).~T(); 
            }
            catch (...) { } 
        }

        /**
         * @brief 
         *  Function reserve will allocate specified space in memory.
         * 
         * @param new_capacity is the new capacity that will be used as a reference 
         *  point for reallocating
         */
        void reserve(size_t new_capacity) {
            if (new_capacity <= capacity_) return; // No need for allocation
            
            // new allocated memory for data
            T* data = (T*)::operator new(new_capacity * sizeof(T)); 
            size_t i = 0;
            
            try {
                for ( ; i < size_; i++)
                    new (data + i) T(std::move_if_noexcept(this->data()[i]));
            } // get the array to default state if it fails
            catch(...){ 
                for ( ; i > 0; i--) 
                    (data + i - 1)->~T(); 
                ::operator delete(data); 
                throw; 
            }
            
            // destroy not needed memmory 
            clear(); 
            ::operator delete(data_);

            // swap variables 
            data_ = data; 
            capacity_ = new_capacity; 
            size_ = i;
        }

        /**
         * @brief change the size of the vector
         * 
         * @param size wanted size 
         * @param value default value for the new elements
         */
        void resize(size_t size, const value_type& value = value_type()){
            if(size_ == size) return; // if equal return

            // the size is smaller => delete elements from size to size_ 
            if(size < size_){ 
                for (size_t i = size; i < size_; i++)
                    (data()[i]).~T();
            } else if(size > size_){ // allocate new data and copy the value to the new mem
                reserve(size);
                for(size_t i = size_; i < size; i++)
                    new (data() + i) T(value);
            }
            size_ = size;
        }

        /**
         * @brief check size, make new elements and add to the last position
         * 
         * @param value 
         */
        void push_back(const T& value) {
            if (size_ == capacity_) 
                reserve(capacity_ == 0 ? 1 : 2 * capacity_);
            new (data() + size_) T(value);
            size_++; 
        }

        /**
         * @brief check size, make new elements and add to the last position
         * 
         * @param value pushed value
         */
        void push_back(T&& value) {
            if (size_ == capacity_) 
                reserve(capacity_ == 0 ? 1 : 2 * capacity_);
            new (data() + size_) T(std::move(value));            
            size_++;     
        }

        /**
         * @brief forwarding with emplace back, it will add the new elements 
         * 
         * @tparam Ts 
         * @param params 
         */
        template <typename... Ts> void emplace_back(Ts&&... params) {
            if (size_ == capacity_) 
                reserve(capacity_ == 0 ? 1 : 2 * capacity_);
            
            new (data() + size_) T(std::forward<Ts>(params)...); size_++;
        } 

        // Getting methods for various data
        size_t size() const{ return size_; }
        size_t capacity() const{ return capacity_; }
        T * data(){ return (data_) ? data_ : reinterpret_cast<T*>(buff_); }
        T * data() const{ return (data_) ? data_ : reinterpret_cast<const T*>(buff_); }

        // Iterators 
        T * begin(T& obj){ return data();}
        T * begin() const{ return data(); }
        T * end(){ return &data()[size_]; }
        T * end() const{ return &data()[size_]; }
        
        ~small_vector(){
            clear(); 
            delete data_; // free allocated memory 
            capacity_ = size_ = 0; // reset the variables
        }

    private:
        T * data_;                // elements of vector, must be also align
        size_t capacity_, size_; // actual capacity and size  
        alignas(alignof(T)) char buff_[N*sizeof(T)];
    };

}


#endif
