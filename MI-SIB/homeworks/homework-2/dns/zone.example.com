$TTL 86400
@   IN  SOA     dns1.example.com. dns2.example.com. (
        2011071001  ;Serial
        3600        ;Refresh
        1800        ;Retry
        604800      ;Expire
        86400       ;Minimum TTL
)
@       IN  NS          dns1.example.com.
@       IN  NS          dns2.example.com.
@       IN  A           192.168.1.101
@       IN  A           192.168.1.102
@       IN  A           192.168.1.103
dns1    IN  A   192.168.1.101
dns2    IN  A   192.168.1.102
client  IN  A   192.168.1.103
