#!/usr/bin/env bash

set -e
set -x

# root check
if [ "$EUID" -ne 0 ]; then
    echo "Please run as root." >&2
    exit 1
fi

echo "Deleting virtual interface 'sib-virtual2'"
ip link delete sib-virtual2 type dummy

echo "=== SUCCESS ==="