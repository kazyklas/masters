## Prerequisites

Please make sure you have Vagrant and Virtualbox installed on your host OS.

## Scripts

Please run the following scripts in this exact order.

1) `create-interface.sh`. Creates a virtual network 'sib-virtual' with ip 10.0.13.1/24. **Please run as sudo.**
2) `start.sh`. Starts the virtual machines.
3) `stop.sh`. It will stop running instances of the virtual machines.
4) `destroy.sh`. The script destroys virtual machine instances.
5) `delete-interface.sh`. Deletes virtual network 'sib-virtual'. **Please run as sudo.**

## Virtual machines

* **DNS server** runs with IP `10.0.14.2`.
* **Attacker** runs with IP `10.0.14.4`.
To start sending TCP SYN packets, run `sudo /vagrant/syn-flood.py`. The server's address is hardcoded in the script.
To start sending DNS multiplication packets, run `sudo /vagrant/dns-multiplication.py`. The servers' addresses are hardcoded in the script.
* **HTTP server** runs with IP `10.0.14.3`.
To monitor TCP SYN and DNS multiplication packets with our Python script, run `sudo /vagrant/monitor.py enp0s8`.
To monitor TCP SYN and DNS multiplication packets with our C program, compile it with `gcc -o monitor -lpcap /vagrant/monitor.c`.
If for some reason that does not work, please try installing libpcap-devel. Afterwards, you can monitor the desired packets with `sudo ./monitor enp0s8`
