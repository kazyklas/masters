#!/usr/bin/env bash

# fail on error, log commands
set -e
set -x

# root check
if [ "$EUID" -ne 0 ]; then
    echo "Please run as root." >&2
    exit 1
fi

echo "Creating virtual interface 'sib-virtual2' with IP 10.0.14.1/24"
ip link add sib-virtual2 type dummy
ip address change dev sib-virtual2 10.0.14.1/24

echo "Interface created successfully"