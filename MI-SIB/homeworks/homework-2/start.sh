#!/usr/bin/env bash

set -e
set -x

SCRIPT_DIR="$(readlink -e ${0%/*})"

echo "Starting DNS server (10.0.14.2)"
cd "${SCRIPT_DIR}/dns"
vagrant up
cd -

echo "Starting HTTP server (10.0.14.3)"
cd "${SCRIPT_DIR}/server"
vagrant up
cd -

echo "Starting attacker (10.0.14.4)"
cd "${SCRIPT_DIR}/attacker"
vagrant up
cd -

echo "=== STARTED SUCCESSFULLY ==="