#!/usr/bin/env bash

set -e
set -x

SCRIPT_DIR="$(readlink -e ${0%/*})"

echo "Stopping DNS server (10.0.14.2)"
cd "${SCRIPT_DIR}/dns"
vagrant halt
cd -

echo "Stopping HTTP server (10.0.14.3)"
cd "${SCRIPT_DIR}/server"
vagrant halt
cd -

echo "Stopping attacker (10.0.14.4)"
cd "${SCRIPT_DIR}/attacker"
vagrant halt
cd -

echo "=== STOPPED SUCCESSFULLY ==="