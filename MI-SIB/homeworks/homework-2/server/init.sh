#!/usr/bin/env bash

set -e
set -x

cp /vagrant/http-server.py /home/miesib
chmod +x /home/miesib/http-server.py
chown miesib /home/miesib/http-server.py

echo 'net.ipv4.tcp_syncookies = 0' >> /etc/sysctl.conf
sysctl --system

firewall-cmd --add-port=8000/tcp --permanent
firewall-cmd --reload

cp /vagrant/http-server.service /etc/systemd/system/http-server.service

systemctl daemon-reload
systemctl start http-server.service
systemctl enable http-server.service
yum install -y libpcap-devel

