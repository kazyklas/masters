#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAXBYTES2CAPTURE 1024
/* ethernet headers are always exactly 14 bytes */
#define SIZE_ETHERNET 14

typedef struct {
    size_t monitored;
    size_t all;
} pkt_stats;

/* Ethernet addresses are 6 bytes */
#define ETHER_ADDR_LEN  6

// BEGIN CODE STOLEN FROM https://www.tcpdump.org/pcap.html
/* Ethernet header */
struct sniff_ethernet {
    u_char ether_dhost[ETHER_ADDR_LEN]; /* Destination host address */
    u_char ether_shost[ETHER_ADDR_LEN]; /* Source host address */
    u_short ether_type; /* IP? ARP? RARP? etc */
} __attribute__((__packed__));

/* IP header */
struct sniff_ip {
    u_char ip_vhl;      /* version << 4 | header length >> 2 */
    u_char ip_tos;      /* type of service */
    u_short ip_len;     /* total length */
    u_short ip_id;      /* identification */
    u_short ip_off;     /* fragment offset field */
#define IP_RF 0x8000        /* reserved fragment flag */
#define IP_DF 0x4000        /* don't fragment flag */
#define IP_MF 0x2000        /* more fragments flag */
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
    u_char ip_ttl;      /* time to live */
    u_char ip_p;        /* protocol */
    u_short ip_sum;     /* checksum */
    struct in_addr ip_src;
    struct in_addr ip_dst; /* source and dest address */
} __attribute__((__packed__));
#define IP_HL(ip)       (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)        (((ip)->ip_vhl) >> 4)

/* TCP header */
typedef u_int tcp_seq;

struct sniff_tcp {
    u_short th_sport;   /* source port */
    u_short th_dport;   /* destination port */
    tcp_seq th_seq;     /* sequence number */
    tcp_seq th_ack;     /* acknowledgement number */
    u_char th_offx2;    /* data offset, rsvd */
#define TH_OFF(th)  (((th)->th_offx2 & 0xf0) >> 4)
    u_char th_flags;
#define TH_FIN 0x01
#define TH_SYN 0x02
#define TH_RST 0x04
#define TH_PUSH 0x08
#define TH_ACK 0x10
#define TH_URG 0x20
#define TH_ECE 0x40
#define TH_CWR 0x80
#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
    u_short th_win;     /* window */
    u_short th_sum;     /* checksum */
    u_short th_urp;     /* urgent pointer */
} __attribute__((__packed__));
// END CODE STOLEN FROM https://www.tcpdump.org/pcap.html

struct sniff_udp {
    u_short srcport;
    u_short dstport;
    u_short length;
    u_short checksum;
} __attribute__((__packed__));

pcap_t *descr = NULL;  // descriptor used by pcap_loop

void process_tcp_syn(const u_char* packet, const struct sniff_ip *ip, u_int size_ip, pkt_stats *stats) {
    const struct sniff_tcp *tcp = (struct sniff_tcp*)(packet + SIZE_ETHERNET + size_ip);
    u_int size_tcp = TH_OFF(tcp)*4;
    if (size_tcp < 20) {
        fprintf(stderr, "   * Invalid TCP header length: %u bytes\n", size_tcp);
        return;
    }


    if (tcp->th_flags & TH_SYN) {
        char srcaddr[16] = {0};
        char dstaddr[16] = {0};
        strcpy(srcaddr, inet_ntoa(ip->ip_src));
        strcpy(dstaddr, inet_ntoa(ip->ip_dst));
        printf("TCP SYN %s:%hu -> %s:%hu\n", srcaddr, ntohs(tcp->th_sport), dstaddr, ntohs(tcp->th_dport));
        ++stats->monitored;
        printf("monitored packets: %lu, total packets: %lu\n", stats->monitored, stats->all);
    }
}

void process_dns_rr(const u_char* packet, const struct sniff_ip *ip, u_int size_ip, pkt_stats *stats) {
    const struct sniff_udp *udp = (struct sniff_udp*)(packet + SIZE_ETHERNET + size_ip);
    if (ntohs(udp->srcport) == 53) {
        // we most likely got a DNS response
        char srcaddr[16] = {0};
        char dstaddr[16] = {0};
        strcpy(srcaddr, inet_ntoa(ip->ip_src));
        strcpy(dstaddr, inet_ntoa(ip->ip_dst));
        printf("DNS resp %s:%hu -> %s:%hu\n", srcaddr, ntohs(udp->srcport), dstaddr, ntohs(udp->dstport));
        ++stats->monitored;
        printf("monitored packets: %lu, total packets: %lu\n", stats->monitored, stats->all);
    }
}

/** Print some characteristics of each packet */
void process_packet(u_char *arg, const struct pcap_pkthdr* hdr, const u_char* packet) {
    const struct sniff_ip *ip; /* The IP header */
    u_int size_ip = 0;
    pkt_stats *stats = (pkt_stats*) arg;
    int ptype = pcap_datalink(descr);

    ++stats->all;

    if (ptype != DLT_EN10MB) {
        // not ethernet (at least not the most common one)
        return;
    }

    ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
    size_ip = IP_HL(ip)*4;
    if (size_ip < 20) {
        // Invalid IP header length or not IP at all
        return;
    }

    /* determine protocol */    
    switch(ip->ip_p) {
        case IPPROTO_TCP:
            process_tcp_syn(packet, ip, size_ip, stats);
            break;
        case IPPROTO_UDP:
            process_dns_rr(packet, ip, size_ip, stats);
            break;
        default:
            printf("untracked protocol\n");
            break;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2 || strlen(argv[1]) > 20) {
        // TODO: is the length check really necessary?
        fprintf(stderr, "Usage: %s <iface>\nmax length of iface is 20", argv[0]);
        return 1;
    }
    const char * const iface = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];  // if failed, contains the error text
    memset(errbuf, 0, PCAP_ERRBUF_SIZE);  // errbuf initialized

    // Open device in promiscuous mode
    descr = pcap_open_live(iface, MAXBYTES2CAPTURE, 1, 512, errbuf);

    if (!descr) {
        fprintf(stderr, "pcap_open_live failed: %s\n", errbuf);
        return 1;
    }

    int *dlt_buf = NULL;
    // Enumerate the data link types, and display human-readable names and descriptions for them
    int num = pcap_list_datalinks(descr, &dlt_buf);
    int ii;
    for (ii = 0; ii < num; ++ii) {
        printf("%d - %s - %s\n\n",dlt_buf[ii],
        pcap_datalink_val_to_name(dlt_buf[ii]),
        pcap_datalink_val_to_description(dlt_buf[ii]));
    }
    pcap_free_datalinks(dlt_buf);

    pkt_stats stats = {0};

    // Start infinite packet processing loop
    pcap_loop(descr, -1, process_packet, (u_char *) &stats);

    // Close the descriptor of the opened device
    pcap_close(descr);

    return 0;
}
