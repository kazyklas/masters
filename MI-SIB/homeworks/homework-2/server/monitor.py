#!/usr/bin/python3

from scapy.all import *
from sys import argv, stderr, stdout

if len(argv) != 2 and len(argv) != 3:
	print("Usage: " + argv[0] + " <iface> [port]", file=stderr)
	print("port is an optional parameter for TCP SYN packets", file=stderr)
	exit(1)

port_filt = ""

if len(argv) == 3:
	port_filt = argv[2]

packets_total = 0
packets_monitored = 0

def is_syn(x):
	return x.haslayer(TCP) and (port_filt == "" or x[TCP].dport == port_filt or x[TCP].sport == port_filt) and "S" in x[TCP].flags

def is_dnsrr(x):
	return x.haslayer(DNSRR)

def print_payload(x):
	global packets_total, packets_monitored
	packets_total += 1
	if is_syn(x) or is_dnsrr(x):
		packets_monitored += 1
		print(x.summary())
		print("total packets: " + str(packets_total) + ", monitored packets: " + str(packets_monitored))

sniff(iface=argv[1], prn=print_payload)
