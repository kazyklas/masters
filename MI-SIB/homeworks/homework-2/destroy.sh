#!/usr/bin/env bash

# fail on error, log commands
set -e
set -x

SCRIPT_DIR="$(readlink -e ${0%/*})"

echo "Destroying DNS server (10.0.14.2)"
cd "${SCRIPT_DIR}/dns"
vagrant destroy -f
cd -

echo "Destroying HTTP server (10.0.14.3)"
cd "${SCRIPT_DIR}/server"
vagrant destroy -f
cd -

echo "Destroying attacker (10.0.14.4)"
cd "${SCRIPT_DIR}/attacker"
vagrant destroy -f
cd -

echo "=== SUCCESS ==="