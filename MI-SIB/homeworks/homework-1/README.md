# ARP Cache Poisoning

This is an implementation of the first homework for ARP cache poisoning.
The following scripts will start three virtual machines (telnet server, telnet client, MiTM server) on a virtual network.

## Prerequisites

Please make sure you have Vagrant and Virtualbox installed on your host OS.

## Scripts

Please run the following scripts in this exact order.

1) `create-interface.sh`. Creates a virtual network 'sib-virtual' with ip 10.0.13.1/24. **Please run as sudo.**
2) `start.sh`. Starts the virtual machines. The script starts the server and the client in the background. The MiTM will run in the foreground.
The arp cache poisoning will start automatically when the script finishes successfully. *Note: it may take several minutes to start everything.*
3) `stop.sh`. It will stop running instances of the virtual machines.
4) `destroy.sh`. The script destroys virtual machine instances.
5) `delete-interface.sh`. Deletes virtual network 'sib-virtual'. **Please run as sudo.**

## Virtual machines

* **Telnet server** runs with IP `10.0.13.2`.
* **Telnet client** runs with IP `10.0.13.3`. To use the client, go to directory `client` and run command `vagrant ssh`.
It is possible to connect to the server via command `telnet 10.0.13.2`. Username and password is `miesib`.
* **Attacker** runs with IP `10.0.13.4`. On the machine is installed service `arp-poison.service` that automatically poisons the arp cache of machines on the segment.
The logs of the service are accessible via command `journalctl -u arp-poison.service -f`. To start Wireshark recording run `sudo wireshark`. The poison script is accessible in directory `attack-service` located in home of `miesib` user.

To start monitoring a telnet connection from the attacker's machine, run `sudo /vagrant/monitor.py enp0s8`. This will print out packet summaries of the connection.

To duplicate the client's terminal, do `sudo /vagrant/capture-telnet.py enp0s8`. The output of the program is pretty straightforward.

## Screenshot

This is a screenshot wireshark recording of attacker machine. On the picture there are intercepted telnet packets.

![Wireshark screenshot](wireshark-screenshot.png)