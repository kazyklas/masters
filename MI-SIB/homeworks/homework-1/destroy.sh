#!/usr/bin/env bash

# fail on error, log commands
set -e
set -x

SCRIPT_DIR="$(readlink -e ${0%/*})"

echo "Destroying server (10.0.13.2)"
cd "${SCRIPT_DIR}/server"
vagrant destroy -f
cd -

echo "Destroying client (10.0.13.3)"
cd "${SCRIPT_DIR}/client"
vagrant destroy -f
cd -

echo "Destroying MiTM (10.0.13.4)"
cd "${SCRIPT_DIR}/attacker"
vagrant destroy -f
cd -

echo "=== SUCCESS ==="