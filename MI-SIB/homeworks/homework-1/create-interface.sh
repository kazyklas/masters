#!/usr/bin/env bash

# fail on error, log commands
set -e
set -x

# root check
if [ "$EUID" -ne 0 ]; then
    echo "Please run as root." >&2
    exit 1
fi

echo "Creating virtual interface 'sib-virtual' with IP 10.0.13.1/24"
ip link add sib-virtual type dummy
ip address change dev sib-virtual 10.0.13.1/24

echo "Interface created successfully"