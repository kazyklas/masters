#!/usr/bin/env bash

set -e
set -x

SCRIPT_DIR="$(readlink -e ${0%/*})"

echo "Stopping server (10.0.13.2)"
cd "${SCRIPT_DIR}/server"
vagrant halt
cd -

echo "Stopping client (10.0.13.3)"
cd "${SCRIPT_DIR}/client"
vagrant halt
cd -

echo "Stopping MiTM (10.0.13.4)"
cd "${SCRIPT_DIR}/attacker"
vagrant halt
cd -

echo "=== STOPPED SUCCESSFULLY ==="