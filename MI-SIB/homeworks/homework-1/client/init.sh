#!/usr/bin/env bash

set -e
set -x

echo "Install telnet client."
yum -y install telnet

echo "=== SUCCESS ==="