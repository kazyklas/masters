#!/usr/bin/env bash

set -e
set -x

SCRIPT_DIR="$(readlink -e ${0%/*})"

echo "Starting server (10.0.13.2)"
cd "${SCRIPT_DIR}/server"
vagrant up
cd -

echo "Starting client (10.0.13.3)"
cd "${SCRIPT_DIR}/client"
vagrant up
cd -

echo "Starting MiTM (10.0.13.4)"
cd "${SCRIPT_DIR}/attacker"
vagrant up
cd -

echo "=== STARTED SUCCESSFULLY ==="