#!/usr/bin/python3

from scapy.all import *
from sys import argv, stderr, stdout

if len(argv) != 2:
	print("Usage: " + argv[0] + " <iface>", file=stderr)
	exit(1)

server_ip = '10.0.13.2'
server_mac = getmacbyip(server_ip)

def print_payload(x):
	return x.summary()

sniff(iface=argv[1], filter="port 23", prn=print_payload)
