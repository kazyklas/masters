#!/usr/bin/env bash

set -e
set -x

echo "Disabling firewall (because we do not want to drop any packets)."
systemctl stop firewalld
systemctl disable firewalld

echo "Installing python packages..."
pip3 install scipy

echo "Installing ARP poison service"
DIR="/home/miesib/attack-service"
mkdir $DIR

cp /vagrant/arp-poison.py $DIR
chmod +x "$DIR/arp-poison.py"
cp /vagrant/arp-poison.service /etc/systemd/system/arp-poison.service

systemctl daemon-reload
systemctl start arp-poison.service
systemctl enable arp-poison.service

echo "=== SUCCESS ==="
