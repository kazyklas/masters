#!/usr/bin/env bash

set -e
set -x

echo "Installing telnet server."
yum -y install telnet-server telnet
firewall-cmd --add-port=23/tcp --permanent
firewall-cmd --reload
systemctl start telnet.socket
systemctl enable telnet.socket

echo "=== SUCCESS ==="