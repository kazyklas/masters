#include <bits/stdc++.h>

using namespace std;

int counter;

//find actual value of backpack and next elements
int sum(vector<int> value, int count, int actualValue) {
    int sum = 0;
    sum += actualValue;
    for (int i = count; i > 0; i--)
        sum += value[i - 1];
    return sum;
}

bool backpackBnB(int backpackSize, vector<int> &weight, vector<int> &value, int count, int minimalValue, int actualValue) {
    if (backpackSize >= 0 && actualValue >= minimalValue)
        return true;
    if (count == 0 || backpackSize == 0 || sum(value, count, actualValue) < minimalValue) // branch and baund
        return false;
    if (count > 0) {
        if (weight[count - 1] > backpackSize) {
            if (backpackBnB(backpackSize, weight, value, count - 1, minimalValue, actualValue)) {
                return true;
            }
        } else {
            counter++;
            if (backpackBnB(backpackSize - weight[count - 1], weight, value, count - 1, minimalValue,
                         actualValue + value[count - 1]))
                return true;
            if (backpackBnB(backpackSize, weight, value, count - 1, minimalValue, actualValue))
                return true;
        }
    }
    return false;
}

bool batohBrute(int backpackSize, vector<int> &weight, vector<int> &value, int count, int minimalValue, int actualValue) {
    if (backpackSize >= 0 && actualValue >= minimalValue)
        return true;
    if (count == 0 || backpackSize == 0)
        return false;
    if (count > 0) {
        if (weight[count - 1] > backpackSize) {
            if (batohBrute(backpackSize, weight, value, count - 1, minimalValue, actualValue)) {
                return true;
            }
        } else {
            counter++;
            if (batohBrute(backpackSize - weight[count - 1], weight, value, count - 1, minimalValue,
                           actualValue + value[count - 1]))
                return true;
            if (batohBrute(backpackSize, weight, value, count - 1, minimalValue, actualValue))
                return true;
        }
    }
    return false;
}

// Driver code 
int main() {
    // read the input file  
    fstream file;
    file.open("ZR15_inst.dat");

    // variable declaration 
    int id, count, backpackSize, minimalValue, result;
    long unsigned int avarage = 0, maximum = 0;
    vector<int> weight, value;
    int tmp;
        
    while (true) {
        //read the variables
        file >> id >> count >> backpackSize >> minimalValue;
        if (file.eof())
            break;
       
        //insert into the array
        for (int i = 0; i < count; i++) {
            file >> tmp;
            weight.push_back(tmp);
            file >> tmp;
            value.push_back(tmp);
        }

        counter = 1;

        result = backpackBnB(backpackSize, weight, value, count, minimalValue, 0);

        avarage += counter;
        if (counter > maximum)
            maximum = counter;
        cout << "Result:\t" << result << "\tconfiguration count:\t" << counter << endl;
    }
   
    avarage /= 500;
    cout << endl << "Avarage: " << avarage << endl << "Max: " << maximum << endl;
    
    file.close();
   
    return 0;
} 
