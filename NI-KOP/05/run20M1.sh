#!/bin/bash

mkdir -p ./plotM1

for f in /home/slave/Documents/kop_5_data/wuf-M1/wuf20-78-M1/*
do
    filename=$(basename $f '.mwcnf')

    result=$(./5_uloha < $f 2>./plotM1/$filename.dat | cut -d ' ' -f 1)

    echo $result $(grep "${filename:1} " /home/slave/Documents/kop_5_data/wuf-M1/wuf20-78-M-opt.dat | head -n 1 | cut -d ' ' -f 2) $filename

done
