#!/bin/bash

letter="R"

mkdir -p ./plot${letter}1

for f in /home/slave/Documents/kop_5_data/wuf-${letter}1/wuf20-78-${letter}1/*
do
    filename=$(basename $f '.mwcnf')

    result=$(./5_uloha < $f 2>./plot${letter}1/$filename.dat | cut -d ' ' -f 1)

    echo $result $(grep "${filename:1} " /home/slave/Documents/kop_5_data/wuf-${letter}1/wuf20-78-${letter}-opt.dat | head -n 1 | cut -d ' ' -f 2) $filename

done
