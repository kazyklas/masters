from sklearn.model_selection import ParameterGrid
import numpy as np
import multiprocessing
from joblib import Parallel, delayed
from tqdm import tqdm
from subprocess import Popen, PIPE

best = None
total_price = 75 + 54 + 65 + 2865 + 2352 + 2713 + 49820 + 116058 + 28173 + 2919 + 2313 + 3993 + 64597 + 57915 + 15901
#total_price = 75 + 54 + 2865 + 2352 + 49820 + 116058 + 2919 + 2313 + 64597 + 57915
best_price = 0
solution_found = False
lock = multiprocessing.Lock()


def custom_popen(pars, filename):
    process = Popen(["./5_uloha", "--population_size", f"{pars['population_size']}",
                     "--number_of_generations", f"{pars['number_of_generations']}",
                     "--crossover_probability", f"{pars['crossover_probability']}",
                     "--mutation_rate", f"{pars['mutation_rate']}",
                     "--elitism", f"{pars['elitism']}"],
                    stdout=PIPE,
                    stderr=PIPE,
                    stdin=open(filename, "r"))

    (output, err) = process.communicate()
    exit_code = process.wait()

    return output, err


def find_params(pars):
    global best
    global best_price
    global total_price
    global solution_found
    global lock

    lock.acquire()
    if solution_found:
        lock.release()
        return
    lock.release()

             # A set
    files = ["/home/slave/Documents/kop_5_data/wuf-A1/wuf20-88-A1/wuf20-014-A.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-A1/wuf20-88-A1/wuf20-0359-A.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-A1/wuf20-88-A1/wuf20-0472-A.mwcnf",

             # M set
             "/home/slave/Documents/kop_5_data/wuf-M1/wuf20-78-M1/wuf20-0512.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-M1/wuf20-78-M1/wuf20-0349.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-M1/wuf20-78-M1/wuf20-0904.mwcnf",

             # N set
             "/home/slave/Documents/kop_5_data/wuf-N1/wuf20-78-N1/wuf20-030.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-N1/wuf20-78-N1/wuf20-0477.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-N1/wuf20-78-N1/wuf20-0943.mwcnf",

             # Q set
             "/home/slave/Documents/kop_5_data/wuf-Q1/wuf20-78-Q1/wuf20-079.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-Q1/wuf20-78-Q1/wuf20-0444.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-Q1/wuf20-78-Q1/wuf20-0700.mwcnf",

             # R set
             "/home/slave/Documents/kop_5_data/wuf-R1/wuf20-78-R1/wuf20-0192.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-R1/wuf20-78-R1/wuf20-0300.mwcnf",
             "/home/slave/Documents/kop_5_data/wuf-R1/wuf20-78-R1/wuf20-0666.mwcnf",
             ]

    num_of_try = 2
    rating_sum = 0
    for i in range(num_of_try):
        for f in files:
            (output, err) = custom_popen(pars, f)
            rating_sum += int(output)
    rating = rating_sum / num_of_try

    lock.acquire()
    if rating > best_price:
        best = pars
        best_price = rating

        print(total_price, best_price, (total_price - best_price) / total_price, best)

        if best_price == total_price or best_price == total_price - 1:
            solution_found = True
            print(f'Best params: {pars}')
    lock.release()


if __name__ == '__main__':
    param_grid = {
       'population_size': [i for i in range(400, 1000, 100)],
       'number_of_generations': [i for i in range(400, 1000, 100)],
       'crossover_probability': [i for i in np.arange(0.85, 1.0, 0.05)],
       'mutation_rate': [i for i in np.arange(0.05, 0.50, 0.05)],
       'elitism': [i for i in range(10, 100, 15)]
    }

    # find_params(param_grid)
    num_cores = multiprocessing.cpu_count()
    inputs = tqdm(ParameterGrid(param_grid))

    j = Parallel(n_jobs=num_cores, backend="threading")(delayed(find_params)(p) for p in inputs)

    print("This is best: ", best)
    print("Diff: ", total_price - best_price, total_price, best_price)
