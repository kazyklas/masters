#!/bin/bash

mkdir -p ./plotA1

for f in /home/slave/Documents/kop_5_data/wuf-A1/wuf20-88-A1/*
do
    filename=$(basename $f '.mwcnf')

    result=$(./5_uloha < $f 2>./plotA1/$filename.dat | cut -d ' ' -f 1)

    echo $result $(grep `echo "${filename:1}" | sed 's/^.\(.*\)..$/\1 /'` /home/slave/Documents/kop_5_data/wuf-A1/wuf20-88-A-opt.dat | head -n 1 | cut -d ' ' -f 2) $filename

done
