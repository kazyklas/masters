#!/bin/bash

letter="Q"

mkdir -p ./plot${letter}1medium

for file in `cat /home/slave/Documents/kop_5_data/wuf-${letter}1/wuf50-201-${letter}-opt.dat | cut -d' ' -f 1 | sed 's/\(.*\)/w\1/'| tr '\n' ' '`
do
	path_to_file="/home/slave/Documents/kop_5_data/wuf-${letter}1/wuf50-201-${letter}1/${file}.mwcnf"

	result=$(./5_uloha < $path_to_file 2>./plot${letter}1medium/${file}.dat | cut -d ' ' -f 1)

	echo $result $(grep "${file:1} " /home/slave/Documents/kop_5_data/wuf-${letter}1/wuf50-201-${letter}-opt.dat | head -n 1 | cut -d ' ' -f 2) $file
done
