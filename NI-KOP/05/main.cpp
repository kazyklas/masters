#include <iostream>
#include <random>
#include <array>
#include <algorithm>
#include <sstream>
#include <string>
#include <boost/program_options.hpp>

struct Literal // represents one literal in clause
{
    Literal(int v, bool n): _literal(v), _neg(n) {}

    int _literal;
    bool _neg = false;
};

using vector_type = std::vector<int>;
using clause_type = std::array<Literal, 3>;
using clause_arr_type = std::vector<clause_type>;

class Formula
{
public:
    Formula(vector_type& values, vector_type& weights, clause_arr_type& clause_arr)
    :
        _price(0),
        _rating(0),
        _is_true(false),
        _num_of_false_clause(0),
        _values(values),
        _weights(weights),
        _clause_arr(clause_arr)
    {
        set_values(values);
    }

    Formula& operator=(Formula other)
    {
        swap(other);

        return *this;
    }

    [[nodiscard]] auto get_price() const noexcept
    {
        return _price;
    }

    [[nodiscard]] auto get_rating() const noexcept
    {
        return _rating;
    }

    [[nodiscard]] auto get_values() const noexcept
    {
        return _values;
    }

    Formula create_mutation(double mutation_probability)
    {
        vector_type values = _values;
        for (auto& v: values)
            if (decision(mutation_probability))
                v ^= 1; // flip value

        return Formula(values, _weights, _clause_arr);
    }

    std::tuple<Formula, Formula> create_crossover(const Formula& other)
    {
        //static initialize only at start of the application
        static std::random_device rd;
        static std::mt19937  gen(rd());
        static std::uniform_int_distribution<int> dist(0, _values.size());

        auto first_values = _values;
        auto second_values = other._values;
        auto index = dist(gen); // chose swap-line

        // swap bites
        for (; index < _values.size(); ++index)
            std::swap(first_values[index], second_values[index]);

        return {Formula(first_values, _weights, _clause_arr), Formula(second_values, _weights, _clause_arr)};
    }

    void swap(Formula& other)
    {
        std::swap(_price, other._price);
        std::swap(_rating, other._rating);
        std::swap(_is_true, other._is_true);
        std::swap(_num_of_false_clause, other._num_of_false_clause);
        std::swap(_values, other._values);
        std::swap(_weights, other._weights);
        std::swap(_clause_arr, other._clause_arr);
    }
private:
    int _price;
    int _rating;
    bool _is_true;
    int _num_of_false_clause;
    vector_type _values;
    vector_type& _weights;
    clause_arr_type& _clause_arr; // clause represented by array of size 3

    void calculate_price() noexcept
    {
        _price = 0;

        if (!_is_true)
            return;

        for (vector_type::size_type i = 0; i < _values.size(); ++i)
            _price += _values[i] * _weights[i];
    }

    void calculate_rating() noexcept
    {
        if (_is_true)
            _rating = _price + (int)_clause_arr.size(); // set rating to price + number of clause
        else
            _rating = (int)_clause_arr.size() - _num_of_false_clause;
    }

    [[nodiscard]] bool is_clause_true(const clause_type& clause)  // is clause true
    {
        return std::any_of(clause.begin(),
                           clause.end(),
                           [this](const Literal& l){return _values[l._literal] ^ l._neg; }
                           );
    }

    void set_if_true() // set truthfulness of this formula
    {
        unsigned long is_true = 0;

        for (auto& it: _clause_arr)
            if (is_clause_true(it))
                ++is_true;

        _is_true = is_true == _clause_arr.size(); // all clauses true

        _num_of_false_clause = _clause_arr.size() - is_true;
    }

    void set_values(const vector_type& v)
    {
        _values = v;

        set_if_true();
        calculate_price();
        calculate_rating();
    }

    inline static bool decision(double prob)
    {
        //static initialize only at start of the application
        static std::random_device rd;
        static std::mt19937  gen(rd());
        static std::uniform_real_distribution<double> dist(0.0, 1.0);

        return dist(gen) < prob;
    }
};

class GeneticAlgo
{
public:
    GeneticAlgo(vector_type& weights, clause_arr_type& clause_arr, int num_of_vars)
    :
        _weights(weights),
        _clause_arr(clause_arr),
        _num_of_vars(num_of_vars)
    {}

    std::tuple<int, vector_type> solve(int population_size, int num_of_generations, double crossover_probability, double mutation_rate, int elitism)
    {
        // set params
        _population_size = population_size;

        // start of algorithm
        generate_population();

        for (int i = 0; i < num_of_generations; ++i)
        {
            decltype(_population) new_population;

            sort_population();

            auto elitists = get_elitists(elitism);
            std::cerr << elitists.front().get_price() << std::endl;
            std::move(elitists.begin(), elitists.end(), std::back_inserter(new_population));  // move elitists to new population

            auto pairs = chose_pairs(ceil(((double)population_size - elitism) / 2) );

            for (auto& pair: pairs)
            {
                auto f1 = _population[pair.first], f2 = _population[pair.second];
                auto newF1 = f1;
                auto newF2 = f2;

                if (decision(crossover_probability))
                    std::tie(newF1, newF2) = f1.create_crossover(f2);

                new_population.emplace_back(newF1.create_mutation(mutation_rate));
                if (new_population.size() < population_size)
                    new_population.emplace_back(newF2.create_mutation(mutation_rate));
                else
                    break;
            }

            _population = new_population;
        }

        sort_population();

        auto res = get_elitists(1);
        return {res[0].get_price(), res[0].get_values()};
    }
private:
    vector_type& _weights;
    clause_arr_type& _clause_arr;
    int _num_of_vars, _population_size;
    std::vector<Formula> _population;

    void generate_population()
    {
        std::random_device rd;
        std::mt19937  gen(rd());
        std::uniform_int_distribution<> dist(false, true);

        _population.reserve(_population_size);
        for (int i = 0; i < _population_size; ++i)
        {
            vector_type values(_num_of_vars);
            for (auto& it: values)
                it = dist(gen);

            _population.emplace_back(values, _weights, _clause_arr);
        }
    }

    auto chose_pairs(int len) -> std::vector<std::pair<int, int>>
    {
        std::vector<std::pair<int, int>> pairs;

        if (len <= 0)
            return pairs;

        std::vector<int> ratings;
        ratings.reserve(_population_size);

        for (auto& it: _population)
            ratings.emplace_back(it.get_rating()); // generate ratings of population

        static std::random_device rd;
        static std::mt19937 gen(rd());
        std::discrete_distribution<> d(ratings.begin(), ratings.end());

        pairs.reserve(len);
        for (int i = 0; i < len; ++i)
            pairs.emplace_back(d(gen), d(gen));

        return pairs;
    }
    auto get_elitists(int num_of_elitists) -> std::vector<Formula> // expects that _population is sorted
    {
        std::vector<Formula> elit;

        if (num_of_elitists <= 0)
            return elit;

        if (num_of_elitists > _population_size)
            num_of_elitists = _population_size;

        elit.reserve(num_of_elitists);

        for (int i = 0; i < num_of_elitists; ++i)
            elit.emplace_back(_population[i]);

        return elit;
    }
    void sort_population()
    {
        struct comparator
        {
            inline bool operator()(const Formula& a, const Formula& b) { return a.get_rating() > b.get_rating(); }
        };

        std::sort(_population.begin(),
                  _population.end(),
                  comparator());
    }
    inline static bool decision(double prob)
    {
        //static initialize only at start of the application
        static std::random_device rd;
        static std::mt19937  gen(rd());
        static std::uniform_real_distribution<double> dist(0.0, 1.0);

        return dist(gen) < prob;
    }
};

std::tuple<vector_type, clause_arr_type, int, int> read_input()
{
    vector_type weights;
    clause_arr_type clause_arr;
    int num_of_clause, num_of_vars;
    int read_clause = 0;
    std::string line;

    while (std::getline(std::cin, line))
    {
        std::stringstream ss(line);
        std::string message_kind;

        ss >> message_kind;

        if (message_kind == "c")
            continue;
        else if (message_kind == "p")
        {
            std::string tmp_input;
            ss >> tmp_input;

            ss >> num_of_vars >> num_of_clause;

            --num_of_clause;
        }
        else if (message_kind == "w")
        {
            int val;

            for (int i = 0; i < num_of_vars && ss >> val; ++i)
            {
                weights.emplace_back(val);
            }
        }
        else // clause input started (i read first int of first clause)
        {
            int first = std::stoi(message_kind);
            int second, third, _;

            ss >> second >> third >> _;

            clause_arr.reserve(num_of_clause);

            clause_arr.push_back({Literal(abs(first) - 1, first < 0),
                        Literal(abs(second) - 1, second < 0),
                        Literal(abs(third) - 1, third < 0)});

            ++read_clause;

            if (read_clause >= num_of_clause)
                break;
        }
    }

    return {weights, clause_arr, num_of_clause, num_of_vars};
}

int main(int argc, char** argv)
{
    struct params
    {
        int population_size, number_of_generations;
        double crossover_probability, mutation_rate;
        int elitism;
    } p;

    // parse cmd
    namespace po = boost::program_options;

    po::options_description desc("Allowed options");
    desc.add_options()
            ("population_size", po::value<int>()->default_value(900), "defines size of population")
            ("number_of_generations", po::value<int>()->default_value(700), "defines number of generations")
            ("crossover_probability", po::value<double>()->default_value(0.85), "defined probability of crossover")
            ("mutation_rate", po::value<double>()->default_value(0.1), "defines mutation rate")
            ("elitism", po::value<int>()->default_value(10), "defines how many elitists to keep");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.size() != desc.options().size()) // if not provided all args
    {
        std::cerr << "Not all arguments provided..." << std::endl;
        std::exit(1);
    }

    p = {vm["population_size"].as<int>(),
         vm["number_of_generations"].as<int>(),
         vm["crossover_probability"].as<double>(),
         vm["mutation_rate"].as<double>(),
         vm["elitism"].as<int>()};

    auto [weights, clause_arr, num_of_clause, num_of_vars] = read_input();

    auto solver = GeneticAlgo(weights, clause_arr, num_of_vars);

    auto [price, solution] = solver.solve(p.population_size, p.number_of_generations, p.crossover_probability, p.mutation_rate, p.elitism);

    //std::cout << price << " ";
    //for (auto& el: solution)
    //    std::cout << el << " ";
    //std::cout << std::endl;

    std::cout << price << std::endl;

    return 0;
}
