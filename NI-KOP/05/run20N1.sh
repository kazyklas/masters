#!/bin/bash

mkdir -p ./plotN1

for f in /home/slave/Documents/kop_5_data/wuf-N1/wuf20-78-N1/*
do
    filename=$(basename $f '.mwcnf')

    result=$(./5_uloha < $f 2>./plotN1/$filename.dat | cut -d ' ' -f 1)

    echo $result $(grep "${filename:1} " /home/slave/Documents/kop_5_data/wuf-N1/wuf20-78-N-opt.dat | head -n 1 | cut -d ' ' -f 2) $filename

done
