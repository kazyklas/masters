#include <iostream>
#include <random>
#include <array>
#include <algorithm>
#include <string>
#include <boost/program_options.hpp>
#include <chrono>

using namespace std;

struct Lit {
    Lit(int v, bool n) {
        literal = v;
        negation = n;
    }

    int literal;
    bool negation = false;
};

inline static bool decide(double prob) {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist(0.0, 1.0);
    return dist(gen) < prob;
}

class Formula {
public:
    int price;
    int fitness;
    bool isTrue;
    int numOfFalseClause;
    vector<int> values;
    vector<int> &weights;
    vector<array<Lit, 3>> &clauseArr; // clause represented by array of size 3

    Formula(vector<int> &values, vector<int> &weights, vector<array<Lit, 3>> &clauseArr)
            :
            price(0),
            fitness(0),
            isTrue(false),
            numOfFalseClause(0),
            values(values),
            weights(weights),
            clauseArr(clauseArr) {
        setIfTrue();
        priceFunc();
        fitnessFunc();
    }

    Formula &operator=(Formula other) {
        Formula::swap(other);
        return *this;
    }

    Formula mutate(double mutationProbability) {
        vector<int> valuestmp = values;
        if (decide(mutationProbability)) {
            for (auto &v: valuestmp)
                if (decide(mutationProbability))
                    v ^= 1;
        }
        return Formula(valuestmp, weights, clauseArr);
    }

    tuple<Formula, Formula> crossover(const Formula &other) {
        random_device rd;
        mt19937 gen(rd());
        uniform_int_distribution<int> dist(0, values.size());

        auto firstPair = values;
        auto secondPair = other.values;
        auto index = dist(gen); // chose swap-line

        for (; index < values.size(); ++index)
            std::swap(firstPair[index], secondPair[index]);
        return {Formula(firstPair, weights, clauseArr), Formula(secondPair, weights, clauseArr)};
    }

    void swap(Formula &other) {
        std::swap(price, other.price);
        std::swap(fitness, other.fitness);
        std::swap(isTrue, other.isTrue);
        std::swap(numOfFalseClause, other.numOfFalseClause);
        std::swap(values, other.values);
        std::swap(weights, other.weights);
        std::swap(clauseArr, other.clauseArr);
    }

    void priceFunc() {
        price = 0;
        if (!isTrue)
            return;
        for (vector<int>::size_type i = 0; i < values.size(); ++i)
            price += values[i] * weights[i];
    }

    void fitnessFunc() {
        if (isTrue)
            fitness = price + (int) clauseArr.size(); // set fitness to price + number of clause
        else
            fitness = (int) clauseArr.size() - numOfFalseClause;
    }

    bool isClauseTrue(const array<Lit, 3> &clause) {
        return any_of(clause.begin(),
                      clause.end(),
                      [this](const Lit &l) { return values[l.literal] ^ l.negation; }
        );
    }

    void setIfTrue() {
        unsigned long isTrueTmp = 0;
        for (auto &it: clauseArr)
            if (isClauseTrue(it))
                ++isTrueTmp;
        isTrueTmp = isTrueTmp == clauseArr.size();
        numOfFalseClause = clauseArr.size() - isTrueTmp;
    }


};

class GeneticAlgo {
public:
    vector<int> &weights;
    vector<array<Lit, 3>> &clauseArr;
    int numOfVars, popSize;
    vector<Formula> population;

    GeneticAlgo(vector<int> &weights, vector<array<Lit, 3>> &clauseArr, int numOfVars)
            :
            weights(weights),
            clauseArr(clauseArr),
            numOfVars(numOfVars) {}

    tuple<int, vector<int>>
    solve(int popSize, int numOfGens, double crossProb, double mutateProb, int elitism) {
        popSize = popSize;
        generatePop();

        for (int i = 0; i < numOfGens; ++i) {
            decltype(population) newPop;
            sortPop();
            auto elitists = getElite(elitism);
            cerr << elitists.front().price << endl;
            move(elitists.begin(), elitists.end(), back_inserter(newPop));
            auto pairs = choosePairs(ceil(((double) popSize - elitism) / 2));
            for (auto &pair: pairs) {
                auto f1 = population[pair.first], f2 = population[pair.second];
                auto newF1 = f1;
                auto newF2 = f2;
                if (decide(crossProb))
                    tie(newF1, newF2) = f1.crossover(f2);
                newPop.emplace_back(newF1.mutate(mutateProb));
                if (newPop.size() < popSize)
                    newPop.emplace_back(newF2.mutate(mutateProb));
                else
                    break;
            }
            population = newPop;
        }
        sortPop();
        auto res = getElite(1);
        return {res[0].price, res[0].values};
    }

    auto choosePairs(int len) -> vector<pair<int, int>> {
        vector<pair<int, int>> pairs;
        if (len <= 0)
            return pairs;
        vector<int> fitness;
        fitness.reserve(popSize);
        for (auto &it: population)
            fitness.emplace_back(it.fitness);
        static random_device rd;
        static mt19937 gen(rd());
        discrete_distribution<> d(fitness.begin(), fitness.end());
        pairs.reserve(len);
        for (int i = 0; i < len; ++i)
            pairs.emplace_back(d(gen), d(gen));
        return pairs;
    }

    void generatePop() {
        random_device rd;
        mt19937 gen(rd());
        uniform_int_distribution<> dist(false, true);
        population.reserve(popSize);
        for (int i = 0; i < popSize; ++i) {
            vector<int> values(numOfVars);
            for (auto &it: values)
                it = dist(gen);
            population.emplace_back(values, weights, clauseArr);
        }
    }

    auto getElite(int numOfElite) -> vector<Formula> {
        vector<Formula> elit;
        if (numOfElite <= 0)
            return elit;
        if (numOfElite > popSize)
            numOfElite = popSize;
        elit.reserve(numOfElite);
        for (int i = 0; i < numOfElite; ++i)
            elit.emplace_back(population[i]);
        return elit;
    }

    void sortPop() {
        struct comparator {
            inline bool operator()(const Formula &a, const Formula &b) { return a.fitness > b.fitness; }
        };
        sort(population.begin(),
             population.end(),
             comparator());
    }
};

tuple<vector<int>, vector<array<Lit, 3>>, int, int> readInput() {
    vector<int> weights;
    vector<array<Lit, 3>> clauseArr;
    int numOfClause, numOfVars;
    int readClause = 0;
    string line;

    while (getline(cin, line)) {
        stringstream ss(line); //not a german reference
        string messageStart;
        ss >> messageStart;

        if (messageStart == "c")
            continue;
        else if (messageStart == "p") {
            string input;
            ss >> input;
            ss >> numOfVars >> numOfClause;
            --numOfClause;
        } else if (messageStart == "w") {
            int val;
            for (int i = 0; i < numOfVars && ss >> val; ++i)
                weights.emplace_back(val);
        } else {
            int first = stoi(messageStart);
            int second, third, ending;
            ss >> second >> third >> ending;
            clauseArr.reserve(numOfClause);
            clauseArr.push_back({Lit(abs(first) - 1, first < 0),
                                 Lit(abs(second) - 1, second < 0),
                                 Lit(abs(third) - 1, third < 0)});
            ++readClause;
            if (readClause >= numOfClause)
                break;
        }
    }

    return {weights, clauseArr, numOfClause, numOfVars};
}

int main(int argc, char **argv) {
    struct params {
        int population_size, number_of_generations;
        double crossover_probability, mutation_rate;
        int elitism;
    } p;

    namespace progOps = boost::program_options;

    progOps::options_description desc("Allowed options");
    desc.add_options()
            ("population_size", progOps::value<int>()->default_value(600), "defines size of population")
            ("number_of_generations", progOps::value<int>()->default_value(800), "defines number of generations")
            ("crossover_probability", progOps::value<double>()->default_value(0.85), "defined probability of crossover")
            ("mutation_rate", progOps::value<double>()->default_value(0.15), "defines mutation rate")
            ("elitism", progOps::value<int>()->default_value(10), "defines how many elitists to keep");
    progOps::variables_map vm;
    progOps::store(progOps::parse_command_line(argc, argv, desc), vm);
    progOps::notify(vm);

    if (vm.size() != desc.options().size()) // if not provided all args
    {
        cerr << "Not all arguments provided..." << endl;
        exit(1);
    }

    p = {vm["population_size"].as<int>(),
         vm["number_of_generations"].as<int>(),
         vm["crossover_probability"].as<double>(),
         vm["mutation_rate"].as<double>(),
         vm["elitism"].as<int>()};

    auto[weights, clause_arr, num_of_clause, num_of_vars] = readInput();
    auto solver = GeneticAlgo(weights, clause_arr, num_of_vars);

    auto start = chrono::system_clock::now();
    auto[price, solution] = solver.solve(p.population_size, p.number_of_generations, p.crossover_probability,
                                         p.mutation_rate, p.elitism);
    auto end = chrono::system_clock::now();
    auto time = end - start;
    cout << price << endl;
    cout << time / chrono::milliseconds(1) << endl;

    return 0;
}
