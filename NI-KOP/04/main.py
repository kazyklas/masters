from operator import attrgetter
import numpy as np
import random
import math
import sys


class Jedinec:
    def __init__(self, configurace):
        self.configurace = configurace
        self.cena = 0
        self.normalizovana_cena = 0


class Parametry:
    def __init__(self,
                 velikost_populace,
                 pocet_generaci,
                 pravdepodobnost_reprodukce,
                 pravdepodobnost_retarda,
                 pocet_elitaru):
        self.velikost_populace = velikost_populace
        self.pocet_generaci = pocet_generaci
        self.pravdepodobnost_reprodukce = pravdepodobnost_reprodukce
        self.pravdepodobnost_retarda = pravdepodobnost_retarda
        self.pocet_elitaru = pocet_elitaru


class Algoritmus:

    def __init__(self, n, vahy, ceny, M, parametry):
        self.n = n
        self.vahy = vahy
        self.ceny = ceny
        self.M = M
        self.parametry = parametry

    def normalizuj(self, populace):
        suma = 0
        for i in range(len(populace)):
            suma += populace[i].cena

        for i in range(len(populace)):
            populace[i].normalizovana_cena = populace[i].cena / suma

    def test_instance(self, configurace):
        kapacita = self.M
        for i in range(self.n):
            if configurace[i]:
                kapacita -= self.vahy[i]

        return True if kapacita >= 0 else False

    def reprodukuj(self, osoba1, osoba2):
        range_ = np.arange(self.n)

        index = int(np.random.choice(range_, 1))

        novaOsoba1 = np.concatenate((osoba2.configurace[:index], osoba1.configurace[index:]))
        novaOsoba2 = np.concatenate((osoba1.configurace[:index], osoba2.configurace[index:]))

        return (Jedinec(novaOsoba1), Jedinec(novaOsoba2)) if self.test_instance(novaOsoba1) and self.test_instance(novaOsoba2) else (osoba1, osoba2)

    def mutuj(self, osoba):
        index = np.random.choice(range(self.n), 1)

        nova_konfigurace = osoba.configurace.copy()
        nova_konfigurace[index] ^= 1

        return Jedinec(nova_konfigurace) if self.test_instance(nova_konfigurace) else osoba

    def dostan_elitu(self, populace, pocet_snobu):
        populace.sort(key=attrgetter('normalizovana_cena'), reverse=True)

        return populace[:pocet_snobu]

    def geneticky_algoritmus(self):
        predmety_ke_smazani = []
        for i in range(self.n):
            if self.vahy[i] > self.M:
                predmety_ke_smazani.append(i)
        predmety_ke_smazani.sort(reverse=True)
        for i in predmety_ke_smazani:
            del self.vahy[i]
            del self.ceny[i]
        if self.n == len(predmety_ke_smazani):
            return 0, np.zeros(self.n, dtype=int)
        self.n -= len(predmety_ke_smazani)
        pocet_osob = 0
        populace = []
        vychytavka = 0
        while pocet_osob < self.parametry.velikost_populace:
            nahodna_configurace = np.random.randint(2, size=self.n)
            if self.test_instance(nahodna_configurace):
                populace.append(Jedinec(nahodna_configurace))
            else:
                nahodna_configurace[:] = 0
                nahodna_configurace[vychytavka % self.n] = 1
                vychytavka += 1
                populace.append(Jedinec(nahodna_configurace))
            pocet_osob += 1

        nova_generace = []
        for i in range(self.parametry.pocet_generaci):
            for osoba in populace:
                suma = 0
                for l in range(self.n):
                    if osoba.configurace[l]:
                        suma += self.ceny[l]
                osoba.cena = suma
            self.normalizuj(populace)
            if self.parametry.pocet_elitaru > 0:
                nova_generace = self.dostan_elitu(populace, self.parametry.pocet_elitaru)
            p = []
            pary_k_reprodukci = []
            for j in populace:
                p.append(j.normalizovana_cena)
            indexy = np.random.choice(np.arange(len(populace)), replace=True, p=p, size=self.n)
            for k in range(math.ceil(self.parametry.velikost_populace - len(nova_generace) / 2)):
                match1 = np.random.choice(indexy, 1)
                match2 = np.random.choice(indexy, 1)
                pary_k_reprodukci.append((int(match1), int(match2)))
            for iterator in range(0, len(pary_k_reprodukci)):
                osoba1, osoba2 = populace[pary_k_reprodukci[iterator][0]], populace[
                    pary_k_reprodukci[iterator][1]]
                if random.random() < parametry.pravdepodobnost_reprodukce:
                    dite1, dite2 = self.reprodukuj(osoba1, osoba2)
                    if random.random() < parametry.pravdepodobnost_retarda:
                        nova_generace.append(self.mutuj(dite1))
                        if len(nova_generace) < self.parametry.velikost_populace:
                            nova_generace.append(self.mutuj(dite2))
                    else:
                        nova_generace.append(dite1)
                        if len(nova_generace) < self.parametry.velikost_populace:
                            nova_generace.append(dite2)
                else:
                    nova_generace.append(osoba1)
                    if len(nova_generace) < self.parametry.velikost_populace:
                        nova_generace.append(osoba2)
            populace = nova_generace.copy()
            nova_generace.clear()
        for osoba in populace:
            suma = 0
            for m in range(self.n):
                if osoba.configurace[m]:
                    suma += self.ceny[m]
            osoba.cena = suma
        self.normalizuj(populace)
        nejlepsi_vysledek = self.dostan_elitu(populace, 1)[0]

        return nejlepsi_vysledek.cena, nejlepsi_vysledek.configurace


if __name__ == '__main__':
    parametry = Parametry(500, 200, 0.9, 0.01, 50)
    for line in sys.stdin:
        line.rstrip()
        vahy = []
        ceny = []
        pole_vstup = line.split(" ")
        delka_vstupu = int(pole_vstup[1])
        kapacita_batohu = int(pole_vstup[2])

        for i in range(3, len(pole_vstup)):
            pole_vstup[i] = int(pole_vstup[i])

        for vaha, cena in zip(pole_vstup[3::2], pole_vstup[4::2]):
            vahy.append(vaha)
            ceny.append(cena)

        #print(vahy)
        #print(ceny)

        objekt = Algoritmus(delka_vstupu, vahy, ceny, kapacita_batohu, parametry)
        cena, configurace = objekt.geneticky_algoritmus()
        print(cena, configurace)
