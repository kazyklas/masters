/* A Naive recursive implementation of 
 0-1 batoh(knapsack) problem */
#include <bits/stdc++.h>
#include <chrono>

#include <utility>

using namespace std;
//pocitadlo konfiguraci
int counter =0;

//funkce pro zjisteni aktualni ceny batohu + ceny nerozhodnutych prvku
int sum(vector<int> cena, int pocet, int aktualniCena) {
    int suma = 0;
    suma += aktualniCena;
    for (int i = pocet; i > 0; i--)
        suma += cena[i - 1];
    return suma;
}

int max(int a, int b) { return a > b ? a : b; }

void batohBnB(int velikostBatohu, vector<int> &vaha, vector<int> &cena, int pocet, int &maximalniCena, int aktualniCena,
              vector<int> &maximalniKombinace, vector<int> &aktualniKombinace) {
    if (aktualniCena >= maximalniCena) {
        maximalniCena = aktualniCena;
        maximalniKombinace = aktualniKombinace;
    }
    if (pocet == 0 || sum(cena, pocet, aktualniCena) < maximalniCena || velikostBatohu == 0)
        return;
    if (vaha[pocet - 1] > velikostBatohu) {
        batohBnB(velikostBatohu, vaha, cena, pocet - 1, maximalniCena, aktualniCena, maximalniKombinace,
                 aktualniKombinace);
    } else {
        counter++;
        aktualniKombinace[pocet - 1] = 1;
        batohBnB(velikostBatohu - vaha[pocet - 1], vaha, cena, pocet - 1, maximalniCena, aktualniCena + cena[pocet - 1],
                 maximalniKombinace, aktualniKombinace);
        aktualniKombinace[pocet - 1] = 0;
        batohBnB(velikostBatohu, vaha, cena, pocet - 1, maximalniCena, aktualniCena, maximalniKombinace,
                 aktualniKombinace);
        return;
    }
}

void
batohBrute(int velikostBatohu, vector<int> &vaha, vector<int> &cena, int pocet, int &maximalniCena, int aktualniCena,
           vector<int> &maximalniKombinace, vector<int> &aktualniKombinace) {
    if (aktualniCena >= maximalniCena && velikostBatohu >= 0) {
        maximalniCena = aktualniCena;
        maximalniKombinace = aktualniKombinace;
    }
    if (pocet == 0)
        return;
    aktualniKombinace[pocet - 1] = 1;
    batohBrute(velikostBatohu - vaha[pocet - 1], vaha, cena, pocet - 1, maximalniCena, aktualniCena + cena[pocet - 1],
               maximalniKombinace, aktualniKombinace);
    aktualniKombinace[pocet - 1] = 0;
    batohBrute(velikostBatohu, vaha, cena, pocet - 1, maximalniCena, aktualniCena, maximalniKombinace,
               aktualniKombinace);
}

void batohDekompoziceCena(vector<int> &vaha, vector<int> &cena, int n, int pocet, int aktualniVaha, int aktualniCena,
                          vector<vector<int>> &memo) {
    if (n > pocet)
        return;

    if (memo[aktualniCena][n] != -1 && aktualniVaha > memo[aktualniCena][n])
        return;

    memo[aktualniCena][n] = aktualniVaha;
    batohDekompoziceCena(vaha, cena, n + 1, pocet, aktualniVaha + vaha[n], aktualniCena + cena[n], memo);
    batohDekompoziceCena(vaha, cena, n + 1, pocet, aktualniVaha, aktualniCena, memo);
}

vector<int>
dekomponujCena(vector<int> &vaha, vector<int> &cena, vector<vector<int>> &memo, int pocet, int velikostVektoru,
               int velikostBatohu) {
    vector<int> vysledek(pocet);
    int startovniRadek = 0;
    for (int i = velikostVektoru - 1; i >= 0; i--) {
        if (memo[i][pocet] <= velikostBatohu && memo[i][pocet] != -1) {
            startovniRadek = i;
            break;
        }
    }
    for (int i = pocet - 1; i >= 0; i--) {
        if (memo[startovniRadek][i + 1] == memo[startovniRadek][i])
            vysledek[i] = 0;
        else {
            vysledek[i] = 1;
            startovniRadek -= cena[i];
        }
    }
    return vysledek;
}

class Prvek {
public:
    int vaha, cena, index;

    Prvek(int vaha, int cena, int index) : vaha(vaha), cena(cena), index(index) {}
};

bool seradPrvek(Prvek &a, Prvek &b) {
    return a.cena / a.vaha > b.cena / b.vaha;
}

vector<int> batohGreedy(vector<int> vaha, vector<int> cena, int pocet, int velikostBatohu) {
    vector<Prvek> serazeneVeci;
    vector<int> vysledek(pocet);
    int cenaVysledku = 0;
    int aktualniVaha = 0;
    for (int i = 0; i < pocet; i++) {
        serazeneVeci.emplace_back(vaha[i], cena[i], i);
    }
    sort(serazeneVeci.begin(), serazeneVeci.end(), seradPrvek);
    for (Prvek &a: serazeneVeci) {
        if (aktualniVaha + a.vaha <= velikostBatohu) {
            vysledek[a.index] = 1;
            aktualniVaha += a.vaha;
            cenaVysledku += a.cena;
        } else
            vysledek[a.index] = 0;
    }
    vysledek.push_back(cenaVysledku);
    return vysledek;
}

vector<int> batohGreedyRedux(vector<int> vaha, vector<int> cena, int pocet, int velikostBatohu) {
    vector<int> vysledek = batohGreedy(vaha, cena, pocet, velikostBatohu);
    int cenaVysledku = vysledek.back();
    vysledek.pop_back();
    int indexNejdrazsihoPredmetu = 0;

    for (int i = 1; i < pocet; i++) {
        if (vaha[i] <= velikostBatohu && cena[i] > cena[indexNejdrazsihoPredmetu])
            indexNejdrazsihoPredmetu = i;
    }

    if (cena[indexNejdrazsihoPredmetu] > cenaVysledku) {
        fill(vysledek.begin(), vysledek.end(), 0); //zneplatim nalezenou konfiguraci
        vysledek[indexNejdrazsihoPredmetu] = 1; //oznacim tam pridany pridmet
        cenaVysledku = cena[indexNejdrazsihoPredmetu]; //zmenim cenu vysledku
    }
    vysledek.push_back(cenaVysledku);
    return vysledek;
}

vector<int> batohFPTAS(vector<int> vaha, vector<int> cena, int pocet, int velikostBatohu, double odchylka) {
    int cenaKonfigurace = 0, nejvetsiCena = 0;
    double pocetIgnorovanychBytu;
    for (int i = 0; i < pocet; i++) {
        if (nejvetsiCena < cena[i] && vaha[i] < velikostBatohu)
            nejvetsiCena = cena[i];
    }
    vector<int> cenaNove(cena);
    pocetIgnorovanychBytu = (odchylka * nejvetsiCena) / pocet;
    for (int i = 0; i < pocet; i++) {
        if (pocetIgnorovanychBytu == 0)
            break;
        cenaNove[i] = floor(cena[i] / pocetIgnorovanychBytu);
    }
    int velikostVektoru = accumulate(cenaNove.begin(), cenaNove.end(), 0);
    vector<vector<int>> memo(velikostVektoru + 1, vector<int>(pocet + 1, -1));//pole pro memoizaci
    batohDekompoziceCena(vaha, cenaNove, 0, pocet, 0, 0, memo);
    vector<int> vracenyVektor = dekomponujCena(vaha, cenaNove, memo, pocet, velikostVektoru, velikostBatohu);
    for (int i = 0; i < vracenyVektor.size(); i++)
        if (vracenyVektor[i] == 1) { cenaKonfigurace += cena[i]; }
    vracenyVektor.push_back(cenaKonfigurace);
    return vracenyVektor;
}


string printVector(vector<int> &tmp) {
    string retVal;
    for (int &i : tmp)
        retVal += to_string(i) + " ";
    return retVal;
}

void udelejSumu(vector<int> &cena, vector<int> &sumy) {
    for (int i = 0; i < cena.size(); i++) {
        if (i == 0)
            sumy[i] += cena[i];
        else
            sumy[i] += cena[i] + sumy[i - 1];
    }
}

int main() {
    fstream file, sol;
    ofstream out;
    out.open("../output.txt");
    int id, pocet, velikostBatohu, cenaKonfigurace = 0, pocetMereni = 3, suma = 0, prumernaDobaBehu, pocetInstanci, pocetOK, pocetChyb, vypoctenaOdchylka, cenaReseni, cenaVysledku, cenyChyb, cenyReferenci, solId, solN, solCena, maxDobaBehu;
    double maxOdchylka;
    string reseni, vysledek;
    vector<int> vracenyVektor;
    vector<int> vaha, cena, placeHolder;
    vector<string> jmenaTestovacichSouboru = {"../NK/NK4_inst.dat", "../NK/NK10_inst.dat", "../NK/NK15_inst.dat",
                                              "../NK/NK20_inst.dat", "../NK/NK22_inst.dat",
                                              "../NK/NK25_inst.dat",
                                              "../ZKC/ZKC4_inst.dat", "../ZKC/ZKC10_inst.dat", "../ZKC/ZKC15_inst.dat",
                                              "../ZKC/ZKC20_inst.dat", "../ZKC/ZKC22_inst.dat",
                                              "../ZKC/ZKC25_inst.dat",
                                              "../ZKW/ZKW4_inst.dat", "../ZKW/ZKW10_inst.dat", "../ZKW/ZKW15_inst.dat",
                                              "../ZKW/ZKW20_inst.dat", "../ZKW/ZKW22_inst.dat","../ZKW/ZKW25_inst.dat"};
    vector<string> jmenaTestovacichReseni = {"../NK/NK4_sol.dat", "../NK/NK10_sol.dat", "../NK/NK15_sol.dat",
                                             "../NK/NK20_sol.dat", "../NK/NK22_sol.dat",
                                             "../NK/NK25_sol.dat",
                                             "../ZKC/ZKC4_sol.dat", "../ZKC/ZKC10_sol.dat", "../ZKC/ZKC15_sol.dat",
                                             "../ZKC/ZKC20_sol.dat", "../ZKC/ZKC22_sol.dat",
                                             "../ZKC/ZKC25_sol.dat",
                                             "../ZKW/ZKW4_sol.dat", "../ZKW/ZKW10_sol.dat", "../ZKW/ZKW15_sol.dat",
                                             "../ZKW/ZKW20_sol.dat", "../ZKW/ZKW22_sol.dat","../ZKW/ZKW25_sol.dat"};
    /*
     * "../ZKW/ZKW4_sol.dat", "../ZKW/ZKW10_sol.dat", "../ZKW/ZKW15_sol.dat",
                                             "../ZKW/ZKW20_sol.dat", "../ZKW/ZKW22_sol.dat","../NK/NK4_sol.dat", "../NK/NK10_sol.dat", "../NK/NK15_sol.dat",
                                             "../NK/NK20_sol.dat", "../NK/NK22_sol.dat",
                                             "../NK/NK25_sol.dat",
                                             "../ZKC/ZKC4_sol.dat", "../ZKC/ZKC10_sol.dat", "../ZKC/ZKC15_sol.dat",
                                             "../ZKC/ZKC20_sol.dat", "../ZKC/ZKC22_sol.dat",
                                             "../ZKC/ZKC25_sol.dat",
     */
    //vstup dat pro testovani
    for (int i = 0; i < jmenaTestovacichReseni.size(); i++) {
        file.open(jmenaTestovacichSouboru[i]);
        sol.open(jmenaTestovacichReseni[i]);
        prumernaDobaBehu = 0;
        maxDobaBehu = 0;
        pocetInstanci = 0;
        pocetOK = 0;
        pocetChyb = 0;
        cenyChyb = 0;
        cenyReferenci = 0;
        maxOdchylka = 0;
        while (true) {
            suma = 0;
            reseni.clear();
            //Nacitani prvotnich promennych
            file >> id >> pocet >> velikostBatohu;

            sol >> solId >> solN >> solCena;
            reseni += to_string(solId) + " " + to_string(solN) + " " + to_string(solCena);
            if (file.eof())
                break;
            int tmp;
            vaha.clear();
            cena.clear();
            cenaKonfigurace = 0;
            vracenyVektor.resize(pocet);
            placeHolder.resize(pocet);
            //vlozeni dvojic do vektoru
            for (int i = 0; i < pocet; i++) {
                sol >> tmp;
                reseni += " " + to_string(tmp);
                file >> tmp;
                vaha.push_back(tmp);
                file >> tmp;
                cena.push_back(tmp);
            }

                               // ----------------------------------------------------------------------------------------
                               //KOD PRO SPUSTENI HRUBE SILY NEBO BnB
                               //zde zmenit volany typ reseni problemu batohu (batohBnB/batohBrute)
                               for(int j = 0 ; j < pocetMereni; j++){
                                counter = 0;
                                auto start = chrono::high_resolution_clock::now();
                                batohBrute(velikostBatohu, vaha, cena, pocet, cenaKonfigurace, 0, vracenyVektor, placeHolder);
                                auto konec = chrono::high_resolution_clock::now();
                                auto dobaTrvani = chrono::duration_cast<chrono::microseconds>(konec - start);
                                suma += dobaTrvani.count();
                                vracenyVektor.push_back(cenaKonfigurace);

                               }
                               //----------------------------------------------------------------------------------------
                /*             //DEKOMPOZICE PODLE CENY
                       for (int j = 0; j < pocetMereni; j++) {
                           cenaKonfigurace = 0;
                           int velikostVektoru = accumulate(cena.begin(), cena.end(), 1);
                           vector<vector<int>> memo(velikostVektoru, vector<int>(pocet + 1, -1));//pole pro memoizaci
                           auto start = chrono::high_resolution_clock::now();
                           batohDekompoziceCena(vaha, cena, 0, pocet, 0, 0, memo);
                           vracenyVektor = dekomponujCena(vaha, cena, memo, pocet, velikostVektoru, velikostBatohu);
                           auto konec = chrono::high_resolution_clock::now();
                           auto dobaTrvani = chrono::duration_cast<chrono::microseconds>(konec - start);
                           suma += dobaTrvani.count();
                           for (int i = 0; i < vracenyVektor.size(); i++)
                               if (vracenyVektor[i] == 1) { cenaKonfigurace += cena[i]; }
                           vracenyVektor.push_back(cenaKonfigurace);
                       }
                       //----------------------------------------------------------------------------------------
                      // GREEDY HEURISTIKA
                       for (int j = 0; j < pocetMereni; j++) {
                           auto start = chrono::high_resolution_clock::now();
                           vracenyVektor = batohGreedy(vaha, cena, pocet, velikostBatohu);
                           auto konec = chrono::high_resolution_clock::now();
                           auto dobaTrvani = chrono::duration_cast<chrono::microseconds>(konec - start);
                           suma += dobaTrvani.count();
                       }
                     //----------------------------------------------------------------------------------------
                        //GREEDY REDUX
                        for (int j = 0; j < pocetMereni; j++) {
                           auto start = chrono::high_resolution_clock::now();
                           vracenyVektor = batohGreedyRedux(vaha, cena, pocet, velikostBatohu);
                           auto konec = chrono::high_resolution_clock::now();
                           auto dobaTrvani = chrono::duration_cast<chrono::microseconds>(konec - start);
                           suma += dobaTrvani.count();
                       }
                     //----------------------------------------------------------------------------------------
                        //FPTAS
                        for (int j = 0; j < pocetMereni; j++) {
                           auto start = chrono::high_resolution_clock::now();
                           vracenyVektor = batohFPTAS(vaha, cena, pocet, velikostBatohu, 0.1);
                           auto konec = chrono::high_resolution_clock::now();
                           auto dobaTrvani = chrono::duration_cast<chrono::microseconds>(konec - start);
                           suma += dobaTrvani.count();
                       }*/

            vysledek.clear();
            cenaVysledku = vracenyVektor[pocet];
            vysledek = to_string(abs(id)) + " " + to_string(pocet) + " " + to_string(cenaVysledku) + " ";
            if(maxDobaBehu < suma/pocetMereni)
                maxDobaBehu = suma/pocetMereni;
            prumernaDobaBehu += suma / pocetMereni;
            pocetInstanci++;
            //out << suma / pocetMereni << endl;
           /* vracenyVektor.pop_back();
            for (auto i : vracenyVektor)
                vysledek += to_string(i) + " ";
            vysledek.pop_back();
            if (solCena != cenaVysledku) {
                out << " Chyba " << solId << endl;
                pocetChyb++;
            } else {
                out << " OK" << endl;
                pocetOK++;
            }*/
            cenyChyb += cenaVysledku;
            cenyReferenci += solCena;
            if ((1 - ((double) cenaVysledku / solCena)) >= maxOdchylka)
                maxOdchylka = 1 - ((double) cenaVysledku / solCena);
        }

        file.close();
        sol.close();
        out << "Prumerna_doba_behu:" << prumernaDobaBehu / pocetInstanci << ":";
        out << "Maximalni_doba_behu:" << maxDobaBehu << ":";
        double odchylka = 1 - ((double) cenyChyb / cenyReferenci);
        //out << "Odchylka:" << odchylka << ":";
        //out << "Max_odchylka: " << maxOdchylka << endl;
        cout << "KONEC" << jmenaTestovacichSouboru[i] << endl;
    }
    out.close();
    return 0;
} 